module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 16);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/react-fontawesome");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("reactstrap");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = {
  port: 3001,
  TIMEOUT: 30000,
  COOKIE: {
    loginCookie: 'project_cookie',
    loginSession: 'project_session',
    config: {
      maxAge: 60 * 60 * 24 * 365,
      httpOnly: true,
      // http only, prevents JavaScript cookie access
      secure: false // cookie must be sent over https / ssl

    }
  }
};

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/fontawesome");

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(3);
var head__default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: external "reactstrap"
var external__reactstrap_ = __webpack_require__(2);
var external__reactstrap__default = /*#__PURE__*/__webpack_require__.n(external__reactstrap_);

// EXTERNAL MODULE: external "axios"
var external__axios_ = __webpack_require__(4);
var external__axios__default = /*#__PURE__*/__webpack_require__.n(external__axios_);

// EXTERNAL MODULE: ./constant/config.js
var config = __webpack_require__(5);
var config_default = /*#__PURE__*/__webpack_require__.n(config);

// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(1);
var react_fontawesome__default = /*#__PURE__*/__webpack_require__.n(react_fontawesome_);

// CONCATENATED MODULE: ./components/header.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }







var header_Header =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Header, _React$Component);

  function Header() {
    var _this;

    _classCallCheck(this, Header);

    _this = _possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).call(this));
    _this.toggle = _this.toggle.bind(_assertThisInitialized(_this));
    _this.state = {
      name: 'Login',
      islogin: false,
      user_url: '/login',
      dropdownOpen: false
    };
    return _this;
  }

  _createClass(Header, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var headerXhr = external__axios__default.a.create();
      headerXhr.defaults.timeout = config_default.a.TIMEOUT;
      external__axios__default.a.post('/ajax', {
        action: 'header'
      }).then(function (result) {
        return result.data;
      }).then(function (data) {
        if (data.success) {
          _this2.setState({
            name: data.result.firstname,
            islogin: data.success,
            user_url: '#'
          });
        }
      }).catch(function (err) {});
    }
  }, {
    key: "toggle",
    value: function toggle() {
      this.setState({
        dropdownOpen: !this.state.dropdownOpen
      });
    }
  }, {
    key: "render",
    value: function render() {
      var hideNavLinks = false;

      if (this.props.hideNavLinks === 'true') {
        hideNavLinks = true;
      }

      return external__react__default.a.createElement("nav", {
        className: "navbar navbar-expand-lg navbar-light bg-light"
      }, external__react__default.a.createElement("div", {
        className: "container"
      }, external__react__default.a.createElement("a", {
        href: "/",
        className: "navbar-brand"
      }, "Project"), external__react__default.a.createElement("button", {
        className: "navbar-toggler",
        type: "button",
        "data-toggle": "collapse",
        "data-target": "#navbar",
        "aria-controls": "navbarsExample07",
        "aria-expanded": "false",
        "aria-label": "Toggle navigation"
      }, external__react__default.a.createElement("span", {
        className: "navbar-toggler-icon"
      })), external__react__default.a.createElement("div", {
        className: "collapse navbar-collapse",
        id: "navbar"
      }, hideNavLinks === false && external__react__default.a.createElement("ul", {
        className: "navbar-nav mr-auto"
      }, external__react__default.a.createElement("li", {
        className: "nav-item"
      }, external__react__default.a.createElement("a", {
        className: "nav-link",
        href: "#"
      }, "Inactive Link")), external__react__default.a.createElement("li", {
        className: "nav-item dropdown"
      }, external__react__default.a.createElement("a", {
        href: "#",
        className: "nav-link dropdown-toggle",
        id: "dropdown",
        "aria-haspopup": "true",
        "aria-expanded": "false",
        "data-toggle": "dropdown"
      }, "Dropdown"), external__react__default.a.createElement("div", {
        className: "dropdown-menu",
        "aria-labelledby": "dropdown"
      }, external__react__default.a.createElement("a", {
        className: "dropdown-item",
        href: "#"
      }, "Action"), external__react__default.a.createElement("a", {
        className: "dropdown-item",
        href: "#"
      }, "Another action"), external__react__default.a.createElement("a", {
        className: "dropdown-item",
        href: "#"
      }, "Something else here")))), external__react__default.a.createElement("form", {
        className: "form-inline ml-auto form-navigation"
      }, external__react__default.a.createElement("div", {
        className: "form-navigation-wrapper"
      }, external__react__default.a.createElement("span", {
        className: "fa search-area"
      }, external__react__default.a.createElement("span", {
        className: "search-icon"
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "search"
      })), external__react__default.a.createElement("span", {
        className: "form-input-holder"
      }, external__react__default.a.createElement("input", {
        className: "form-control",
        type: "text",
        placeholder: "Search",
        "aria-label": "Search"
      }))), external__react__default.a.createElement(external__reactstrap_["ButtonDropdown"], {
        className: "fa",
        isOpen: this.state.dropdownOpen,
        toggle: this.toggle
      }, !this.state.islogin && external__react__default.a.createElement(external__reactstrap_["DropdownToggle"], {
        href: this.state.user_url
      }, this.state.name), this.state.islogin && external__react__default.a.createElement(external__reactstrap_["DropdownToggle"], {
        caret: true
      }, this.state.name), this.state.islogin && external__react__default.a.createElement(external__reactstrap_["DropdownMenu"], null, external__react__default.a.createElement(external__reactstrap_["DropdownItem"], {
        href: "/settings"
      }, "My account"), external__react__default.a.createElement(external__reactstrap_["DropdownItem"], {
        href: "/logout"
      }, "Logout"))), external__react__default.a.createElement("a", {
        href: "/details",
        className: "fa"
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "cart-plus"
      })))))));
    }
  }]);

  return Header;
}(external__react__default.a.Component);


// EXTERNAL MODULE: external "@fortawesome/fontawesome"
var fontawesome_ = __webpack_require__(6);
var fontawesome__default = /*#__PURE__*/__webpack_require__.n(fontawesome_);

// EXTERNAL MODULE: external "@fortawesome/fontawesome-free-solid"
var fontawesome_free_solid_ = __webpack_require__(8);
var fontawesome_free_solid__default = /*#__PURE__*/__webpack_require__.n(fontawesome_free_solid_);

// CONCATENATED MODULE: ./components/layout.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return layout_Index; });
function layout__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { layout__typeof = function _typeof(obj) { return typeof obj; }; } else { layout__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return layout__typeof(obj); }

function layout__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function layout__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function layout__createClass(Constructor, protoProps, staticProps) { if (protoProps) layout__defineProperties(Constructor.prototype, protoProps); if (staticProps) layout__defineProperties(Constructor, staticProps); return Constructor; }

function layout__possibleConstructorReturn(self, call) { if (call && (layout__typeof(call) === "object" || typeof call === "function")) { return call; } return layout__assertThisInitialized(self); }

function layout__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function layout__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






fontawesome__default.a.library.add(fontawesome_free_solid_["faSearch"], fontawesome_free_solid_["faCartPlus"], fontawesome_free_solid_["faAngleDoubleRight"], fontawesome_free_solid_["faEllipsisH"], fontawesome_free_solid_["faHeart"], fontawesome_free_solid_["faPenSquare"], fontawesome_free_solid_["faCheckCircle"], fontawesome_free_solid_["faExclamationCircle"], fontawesome_free_solid_["faTrashAlt"]);

var layout_Index =
/*#__PURE__*/
function (_Component) {
  layout__inherits(Index, _Component);

  function Index() {
    layout__classCallCheck(this, Index);

    return layout__possibleConstructorReturn(this, (Index.__proto__ || Object.getPrototypeOf(Index)).apply(this, arguments));
  }

  layout__createClass(Index, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", null, external__react__default.a.createElement(head__default.a, null, external__react__default.a.createElement("link", {
        href: "/css/core.css",
        rel: "stylesheet"
      }), external__react__default.a.createElement("link", {
        href: "/css/style.css",
        rel: "stylesheet"
      })), external__react__default.a.createElement(header_Header, null), external__react__default.a.createElement("div", {
        className: "container mc"
      }, this.props.children), external__react__default.a.createElement("script", {
        src: "/js/core.js"
      }));
    }
  }]);

  return Index;
}(external__react_["Component"]);



/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/fontawesome-free-solid");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("react-load-image");

/***/ }),
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

var port = __webpack_require__(5).port;

var BASE_URL = 'http://localhost:' + port;
module.exports = {
  Headers: {},
  Accounts: {
    header: BASE_URL + '/api/user',
    displayName: BASE_URL + '/api/user',
    register: BASE_URL + '/api/register',
    login: BASE_URL + '/api/login'
  },
  Homepage: {
    home: BASE_URL + '/api/products'
  },
  Details: BASE_URL + '/api/product'
};

/***/ }),
/* 15 */,
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(17);


/***/ }),
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(3);
var head__default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: external "axios"
var external__axios_ = __webpack_require__(4);
var external__axios__default = /*#__PURE__*/__webpack_require__.n(external__axios_);

// EXTERNAL MODULE: ./constant/urls.js
var urls = __webpack_require__(14);
var urls_default = /*#__PURE__*/__webpack_require__.n(urls);

// EXTERNAL MODULE: ./components/layout.js + 1 modules
var layout = __webpack_require__(7);

// EXTERNAL MODULE: external "reactstrap"
var external__reactstrap_ = __webpack_require__(2);
var external__reactstrap__default = /*#__PURE__*/__webpack_require__.n(external__reactstrap_);

// EXTERNAL MODULE: external "react-load-image"
var external__react_load_image_ = __webpack_require__(9);
var external__react_load_image__default = /*#__PURE__*/__webpack_require__.n(external__react_load_image_);

// CONCATENATED MODULE: ./components/product-detail/slider-indicator.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }




var slider_indicator_SliderIndicator =
/*#__PURE__*/
function (_React$Component) {
  _inherits(SliderIndicator, _React$Component);

  function SliderIndicator(props) {
    var _this;

    _classCallCheck(this, SliderIndicator);

    _this = _possibleConstructorReturn(this, (SliderIndicator.__proto__ || Object.getPrototypeOf(SliderIndicator)).call(this, props));
    _this.renderSliderIndicator = _this.renderSliderIndicator.bind(_assertThisInitialized(_this));
    _this.onClickHandler = _this.onClickHandler.bind(_assertThisInitialized(_this));
    _this.state = {
      activeIndex: props.activeIndex
    };
    return _this;
  }

  _createClass(SliderIndicator, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      this.setState({
        activeIndex: nextProps.activeIndex
      });
    }
  }, {
    key: "onClickHandler",
    value: function onClickHandler(newIndex) {
      this.setState({
        activeIndex: newIndex
      });
      this.props.onClickHandler(newIndex);
    }
  }, {
    key: "renderSliderIndicator",
    value: function renderSliderIndicator() {
      var _this2 = this;

      var _render = [];
      var thumbnails = this.props.thumbnails;

      var _loop = function _loop(i) {
        var active = i === _this2.state.activeIndex ? 'active' : '';

        _render.push(external__react__default.a.createElement("li", {
          key: i,
          onClick: function onClick() {
            return _this2.onClickHandler(i);
          },
          "data-target": _this2.props.targetId,
          "data-slide-to": i,
          className: active
        }, external__react__default.a.createElement(external__react_load_image__default.a, {
          src: thumbnails[i]
        }, external__react__default.a.createElement("img", null), external__react__default.a.createElement("div", null, "Error!"), external__react__default.a.createElement("img", {
          src: "/img/ajax.gif"
        }))));
      };

      for (var i = 0; i < thumbnails.length; i++) {
        _loop(i);
      }

      return _render;
    }
  }, {
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("ol", {
        className: "carousel-indicators"
      }, this.renderSliderIndicator());
    }
  }]);

  return SliderIndicator;
}(external__react__default.a.Component);


// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(1);
var react_fontawesome__default = /*#__PURE__*/__webpack_require__.n(react_fontawesome_);

// CONCATENATED MODULE: ./components/product-detail/slider-control.js
function slider_control__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { slider_control__typeof = function _typeof(obj) { return typeof obj; }; } else { slider_control__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return slider_control__typeof(obj); }

function slider_control__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function slider_control__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function slider_control__createClass(Constructor, protoProps, staticProps) { if (protoProps) slider_control__defineProperties(Constructor.prototype, protoProps); if (staticProps) slider_control__defineProperties(Constructor, staticProps); return Constructor; }

function slider_control__possibleConstructorReturn(self, call) { if (call && (slider_control__typeof(call) === "object" || typeof call === "function")) { return call; } return slider_control__assertThisInitialized(self); }

function slider_control__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function slider_control__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var slider_control_SliderControl =
/*#__PURE__*/
function (_React$Component) {
  slider_control__inherits(SliderControl, _React$Component);

  function SliderControl() {
    slider_control__classCallCheck(this, SliderControl);

    return slider_control__possibleConstructorReturn(this, (SliderControl.__proto__ || Object.getPrototypeOf(SliderControl)).apply(this, arguments));
  }

  slider_control__createClass(SliderControl, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", null, external__react__default.a.createElement("a", {
        className: "carousel-control-prev",
        href: "javascript:void(0)",
        role: "button",
        onClick: this.props.onPrevClickHandler
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "angle-left"
      })), external__react__default.a.createElement("a", {
        className: "carousel-control-next",
        href: "javascript:void(0)",
        role: "button",
        onClick: this.props.onNextClickHandler
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "angle-right"
      })));
    }
  }]);

  return SliderControl;
}(external__react__default.a.Component);


// CONCATENATED MODULE: ./components/product-detail/slider.js
function slider__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { slider__typeof = function _typeof(obj) { return typeof obj; }; } else { slider__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return slider__typeof(obj); }

function slider__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function slider__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function slider__createClass(Constructor, protoProps, staticProps) { if (protoProps) slider__defineProperties(Constructor.prototype, protoProps); if (staticProps) slider__defineProperties(Constructor, staticProps); return Constructor; }

function slider__possibleConstructorReturn(self, call) { if (call && (slider__typeof(call) === "object" || typeof call === "function")) { return call; } return slider__assertThisInitialized(self); }

function slider__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function slider__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }








var slider_ItemSlider =
/*#__PURE__*/
function (_Component) {
  slider__inherits(ItemSlider, _Component);

  function ItemSlider(props) {
    var _this;

    slider__classCallCheck(this, ItemSlider);

    _this = slider__possibleConstructorReturn(this, (ItemSlider.__proto__ || Object.getPrototypeOf(ItemSlider)).call(this, props));
    _this.state = {
      activeIndex: 0,
      images: props.images
    };
    _this.next = _this.next.bind(slider__assertThisInitialized(_this));
    _this.previous = _this.previous.bind(slider__assertThisInitialized(_this));
    _this.setSliderIndicator = _this.setSliderIndicator.bind(slider__assertThisInitialized(_this));
    _this.onExiting = _this.onExiting.bind(slider__assertThisInitialized(_this));
    _this.onExited = _this.onExited.bind(slider__assertThisInitialized(_this));
    return _this;
  }

  slider__createClass(ItemSlider, [{
    key: "onExiting",
    value: function onExiting() {
      this.animating = true;
    }
  }, {
    key: "onExited",
    value: function onExited() {
      this.animating = false;
    }
  }, {
    key: "next",
    value: function next() {
      if (this.animating) return;
      var nextIndex = this.state.activeIndex === this.props.images.original.length - 1 ? 0 : this.state.activeIndex + 1;
      this.setState({
        activeIndex: nextIndex
      });
    }
  }, {
    key: "previous",
    value: function previous() {
      if (this.animating) return;
      var nextIndex = this.state.activeIndex === 0 ? this.props.images.original.length - 1 : this.state.activeIndex - 1;
      this.setState({
        activeIndex: nextIndex
      });
    }
  }, {
    key: "setSliderIndicator",
    value: function setSliderIndicator(newIndex) {
      if (this.animating) return;
      this.setState({
        activeIndex: newIndex
      });
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(props) {
      this.setState({
        images: props.images
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var activeIndex = this.state.activeIndex;
      var images = Object.keys(this.state.images).length > 0 ? this.state.images : {};
      var slides = [];
      var thumbnails = [];

      if (Object.keys(images).length > 0) {
        slides = images.original.map(function (item, index) {
          return external__react__default.a.createElement(external__reactstrap_["CarouselItem"], {
            className: "text-center",
            onExiting: _this2.onExiting,
            onExited: _this2.onExited,
            key: index
          }, external__react__default.a.createElement(external__react_load_image__default.a, {
            src: item
          }, external__react__default.a.createElement("img", null), external__react__default.a.createElement("div", null, "Error!"), external__react__default.a.createElement("img", {
            src: "/img/ajax.gif"
          })));
        });
        thumbnails = external__react__default.a.createElement(slider_indicator_SliderIndicator, {
          onClickHandler: this.setSliderIndicator,
          activeIndex: activeIndex,
          thumbnails: images.thumbnails,
          targetId: "carousel"
        });
      }

      return external__react__default.a.createElement(external__reactstrap_["Carousel"], {
        id: "carousel",
        activeIndex: activeIndex,
        next: this.next,
        previous: this.previous
      }, slides, thumbnails, external__react__default.a.createElement(slider_control_SliderControl, {
        onPrevClickHandler: this.previous,
        onNextClickHandler: this.next
      }));
    }
  }]);

  return ItemSlider;
}(external__react_["Component"]);


// CONCATENATED MODULE: ./components/product-detail/detail.js
function detail__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { detail__typeof = function _typeof(obj) { return typeof obj; }; } else { detail__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return detail__typeof(obj); }

function detail__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function detail__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function detail__createClass(Constructor, protoProps, staticProps) { if (protoProps) detail__defineProperties(Constructor.prototype, protoProps); if (staticProps) detail__defineProperties(Constructor, staticProps); return Constructor; }

function detail__possibleConstructorReturn(self, call) { if (call && (detail__typeof(call) === "object" || typeof call === "function")) { return call; } return detail__assertThisInitialized(self); }

function detail__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function detail__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var detail_Detail =
/*#__PURE__*/
function (_Component) {
  detail__inherits(Detail, _Component);

  function Detail(props) {
    var _this;

    detail__classCallCheck(this, Detail);

    _this = detail__possibleConstructorReturn(this, (Detail.__proto__ || Object.getPrototypeOf(Detail)).call(this, props));
    _this.state = {
      product: props.product
    };
    return _this;
  }

  detail__createClass(Detail, [{
    key: "render",
    value: function render() {
      var resultsCount = Object.keys(this.state.product).length;
      return external__react__default.a.createElement("div", {
        className: "page"
      }, external__react__default.a.createElement("div", {
        className: "details"
      }, external__react__default.a.createElement("div", {
        className: "row"
      }, external__react__default.a.createElement("div", {
        className: "col-xs-12 col-sm-12 col-md-6 col-lg-6"
      }, external__react__default.a.createElement(slider_ItemSlider, {
        images: this.state.product.images
      })), external__react__default.a.createElement("div", {
        className: "col-xs-12 col-sm-12 col-md-6 col-lg-6"
      }, external__react__default.a.createElement("div", {
        className: "card"
      }, external__react__default.a.createElement("div", {
        className: "card-body"
      }, external__react__default.a.createElement("h2", {
        className: "card-title"
      }, this.state.product.name), external__react__default.a.createElement("h5", {
        className: "card-title"
      }, "Static Label 2"), external__react__default.a.createElement("p", {
        className: "card-text"
      }, this.state.product.description)), external__react__default.a.createElement("div", {
        className: "card-footer product-actions"
      }, external__react__default.a.createElement("div", {
        className: "float-left"
      }, external__react__default.a.createElement("strong", null, "Price: $", this.state.product.price)), external__react__default.a.createElement("div", {
        className: "float-right"
      }, external__react__default.a.createElement("a", {
        href: "#"
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "heart"
      })), external__react__default.a.createElement("a", {
        href: "#"
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "cart-plus"
      })), external__react__default.a.createElement("a", {
        href: "#",
        className: "btn btn-primary"
      }, "Go somewhere"))))))), external__react__default.a.createElement("div", {
        className: "lists"
      }, external__react__default.a.createElement("div", {
        className: "container mc"
      })));
      /*return <div className="page">
          <div className="details">
              <div className="container mc">
                  <div className="row">
                      <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                          <Slider products={this.props.products}></Slider>
                      </div>
                      <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                          <div className="card">
                              <div className="card-body">
                                  <h2 className="card-title">{this.state.product.name}</h2>
                                  <h5 className="card-title">Label 2</h5>
                                  <p className="card-text">
                                      {this.state.product.description}
                                  </p>
                              </div>
                              <div className="card-footer">
                                  <div className="float-left">
                                      <ul className="list-inline">
                                          <li className="list-inline-item">Sizes</li>
                                          <li className="list-inline-item">
                                              <a href="#">S</a>
                                          </li>
                                          <li className="list-inline-item">
                                              <a href="#">M</a>
                                          </li>
                                          <li className="list-inline-item">
                                              <a href="#">L</a>
                                          </li>
                                      </ul>
                                  </div>
                                  <div className="float-right">
                                      <ul className="list-inline">
                                          <li className="list-inline-item">Quantity</li>
                                          <li className="list-inline-item">
                                              <a href="#">+</a>
                                          </li>
                                          <li className="list-inline-item">3</li>
                                          <li className="list-inline-item">
                                              <a href="#">-</a>
                                          </li>
                                      </ul>
                                  </div>
                              </div>
                              <div className="card-footer product-actions">
                                  <div className="float-left">
                                      <strong>Price: $120</strong>
                                  </div>
                                  <div className="float-right">
                                      <a href="#">
                                          <FontAwesomeIcon icon={faHeart}/>
                                      </a>
                                      <a href="#">
                                          <FontAwesomeIcon icon="cart-plus"/>
                                      </a>
                                      <a href="#" className="btn btn-primary">Go somewhere</a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div className="lists">
              <div className="container mc">
              </div>
          </div>
      </div>*/
    }
  }]);

  return Detail;
}(external__react_["Component"]);


// CONCATENATED MODULE: ./pages/detail.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return pages_detail_Detail; });
function pages_detail__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { pages_detail__typeof = function _typeof(obj) { return typeof obj; }; } else { pages_detail__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return pages_detail__typeof(obj); }

function pages_detail__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function pages_detail__possibleConstructorReturn(self, call) { if (call && (pages_detail__typeof(call) === "object" || typeof call === "function")) { return call; } return pages_detail__assertThisInitialized(self); }

function pages_detail__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function pages_detail__createClass(Constructor, protoProps, staticProps) { if (protoProps) pages_detail__defineProperties(Constructor.prototype, protoProps); if (staticProps) pages_detail__defineProperties(Constructor, staticProps); return Constructor; }

function pages_detail__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function pages_detail__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }








var pages_detail_Detail =
/*#__PURE__*/
function (_Component) {
  pages_detail__inherits(Detail, _Component);

  pages_detail__createClass(Detail, null, [{
    key: "getInitialProps",
    value: function getInitialProps(_ref) {
      var _ref$query = _ref.query,
          name = _ref$query.name,
          id = _ref$query.id;
      return {
        name: name,
        id: id
      };
    }
  }]);

  function Detail(props) {
    var _this;

    pages_detail__classCallCheck(this, Detail);

    _this = pages_detail__possibleConstructorReturn(this, (Detail.__proto__ || Object.getPrototypeOf(Detail)).call(this, props));
    _this.fetchDetails = _this.fetchDetails.bind(pages_detail__assertThisInitialized(_this));
    _this.state = {
      product: {},
      fetching: true
    };
    return _this;
  }

  pages_detail__createClass(Detail, [{
    key: "fetchDetails",
    value: function fetchDetails() {
      var _this2 = this;

      external__axios__default.a.get(urls_default.a.Details.concat('?id=eq.').concat(this.props.id).concat('&name=eq.').concat(this.props.name)).then(function (response) {
        return _this2.setState({
          fetching: false,
          product: response.data.result.success
        });
      }).catch(function (ex) {});
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.fetchDetails();
    }
  }, {
    key: "render",
    value: function render() {
      return external__react__default.a.createElement(layout["a" /* default */], null, external__react__default.a.createElement(head__default.a, null, external__react__default.a.createElement("link", {
        href: "/css/detail.css",
        rel: "stylesheet"
      })), external__react__default.a.createElement("div", null, !this.state.fetching && this.state.product.length == 0 && external__react__default.a.createElement("div", {
        className: "row"
      }, external__react__default.a.createElement("div", {
        className: "col-12"
      }, external__react__default.a.createElement("div", {
        className: "alert alert-warning nothing"
      }, "No results found"))), this.state.fetching && external__react__default.a.createElement("div", {
        className: "row"
      }, external__react__default.a.createElement("div", {
        className: "col-12"
      }, external__react__default.a.createElement("div", {
        className: "alert alert-light nothing"
      }, "Loading..."))), Object.keys(this.state.product).length > 0 && external__react__default.a.createElement(detail_Detail, {
        product: this.state.product,
        name: this.props.name,
        id: this.props.id
      })));
    }
  }]);

  return Detail;
}(external__react_["Component"]);



/***/ })
/******/ ]);