module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 28);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/react-fontawesome");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("reactstrap");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = {
  port: 3001,
  TIMEOUT: 30000,
  COOKIE: {
    loginCookie: 'project_cookie',
    loginSession: 'project_session',
    config: {
      maxAge: 60 * 60 * 24 * 365,
      httpOnly: true,
      // http only, prevents JavaScript cookie access
      secure: false // cookie must be sent over https / ssl

    }
  }
};

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/fontawesome");

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(3);
var head__default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: external "reactstrap"
var external__reactstrap_ = __webpack_require__(2);
var external__reactstrap__default = /*#__PURE__*/__webpack_require__.n(external__reactstrap_);

// EXTERNAL MODULE: external "axios"
var external__axios_ = __webpack_require__(4);
var external__axios__default = /*#__PURE__*/__webpack_require__.n(external__axios_);

// EXTERNAL MODULE: ./constant/config.js
var config = __webpack_require__(5);
var config_default = /*#__PURE__*/__webpack_require__.n(config);

// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(1);
var react_fontawesome__default = /*#__PURE__*/__webpack_require__.n(react_fontawesome_);

// CONCATENATED MODULE: ./components/header.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }







var header_Header =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Header, _React$Component);

  function Header() {
    var _this;

    _classCallCheck(this, Header);

    _this = _possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).call(this));
    _this.toggle = _this.toggle.bind(_assertThisInitialized(_this));
    _this.state = {
      name: 'Login',
      islogin: false,
      user_url: '/login',
      dropdownOpen: false
    };
    return _this;
  }

  _createClass(Header, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var headerXhr = external__axios__default.a.create();
      headerXhr.defaults.timeout = config_default.a.TIMEOUT;
      external__axios__default.a.post('/ajax', {
        action: 'header'
      }).then(function (result) {
        return result.data;
      }).then(function (data) {
        if (data.success) {
          _this2.setState({
            name: data.result.firstname,
            islogin: data.success,
            user_url: '#'
          });
        }
      }).catch(function (err) {});
    }
  }, {
    key: "toggle",
    value: function toggle() {
      this.setState({
        dropdownOpen: !this.state.dropdownOpen
      });
    }
  }, {
    key: "render",
    value: function render() {
      var hideNavLinks = false;

      if (this.props.hideNavLinks === 'true') {
        hideNavLinks = true;
      }

      return external__react__default.a.createElement("nav", {
        className: "navbar navbar-expand-lg navbar-light bg-light"
      }, external__react__default.a.createElement("div", {
        className: "container"
      }, external__react__default.a.createElement("a", {
        href: "/",
        className: "navbar-brand"
      }, "Project"), external__react__default.a.createElement("button", {
        className: "navbar-toggler",
        type: "button",
        "data-toggle": "collapse",
        "data-target": "#navbar",
        "aria-controls": "navbarsExample07",
        "aria-expanded": "false",
        "aria-label": "Toggle navigation"
      }, external__react__default.a.createElement("span", {
        className: "navbar-toggler-icon"
      })), external__react__default.a.createElement("div", {
        className: "collapse navbar-collapse",
        id: "navbar"
      }, hideNavLinks === false && external__react__default.a.createElement("ul", {
        className: "navbar-nav mr-auto"
      }, external__react__default.a.createElement("li", {
        className: "nav-item"
      }, external__react__default.a.createElement("a", {
        className: "nav-link",
        href: "#"
      }, "Inactive Link")), external__react__default.a.createElement("li", {
        className: "nav-item dropdown"
      }, external__react__default.a.createElement("a", {
        href: "#",
        className: "nav-link dropdown-toggle",
        id: "dropdown",
        "aria-haspopup": "true",
        "aria-expanded": "false",
        "data-toggle": "dropdown"
      }, "Dropdown"), external__react__default.a.createElement("div", {
        className: "dropdown-menu",
        "aria-labelledby": "dropdown"
      }, external__react__default.a.createElement("a", {
        className: "dropdown-item",
        href: "#"
      }, "Action"), external__react__default.a.createElement("a", {
        className: "dropdown-item",
        href: "#"
      }, "Another action"), external__react__default.a.createElement("a", {
        className: "dropdown-item",
        href: "#"
      }, "Something else here")))), external__react__default.a.createElement("form", {
        className: "form-inline ml-auto form-navigation"
      }, external__react__default.a.createElement("div", {
        className: "form-navigation-wrapper"
      }, external__react__default.a.createElement("span", {
        className: "fa search-area"
      }, external__react__default.a.createElement("span", {
        className: "search-icon"
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "search"
      })), external__react__default.a.createElement("span", {
        className: "form-input-holder"
      }, external__react__default.a.createElement("input", {
        className: "form-control",
        type: "text",
        placeholder: "Search",
        "aria-label": "Search"
      }))), external__react__default.a.createElement(external__reactstrap_["ButtonDropdown"], {
        className: "fa",
        isOpen: this.state.dropdownOpen,
        toggle: this.toggle
      }, !this.state.islogin && external__react__default.a.createElement(external__reactstrap_["DropdownToggle"], {
        href: this.state.user_url
      }, this.state.name), this.state.islogin && external__react__default.a.createElement(external__reactstrap_["DropdownToggle"], {
        caret: true
      }, this.state.name), this.state.islogin && external__react__default.a.createElement(external__reactstrap_["DropdownMenu"], null, external__react__default.a.createElement(external__reactstrap_["DropdownItem"], {
        href: "/settings"
      }, "My account"), external__react__default.a.createElement(external__reactstrap_["DropdownItem"], {
        href: "/logout"
      }, "Logout"))), external__react__default.a.createElement("a", {
        href: "/details",
        className: "fa"
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "cart-plus"
      })))))));
    }
  }]);

  return Header;
}(external__react__default.a.Component);


// EXTERNAL MODULE: external "@fortawesome/fontawesome"
var fontawesome_ = __webpack_require__(6);
var fontawesome__default = /*#__PURE__*/__webpack_require__.n(fontawesome_);

// EXTERNAL MODULE: external "@fortawesome/fontawesome-free-solid"
var fontawesome_free_solid_ = __webpack_require__(8);
var fontawesome_free_solid__default = /*#__PURE__*/__webpack_require__.n(fontawesome_free_solid_);

// CONCATENATED MODULE: ./components/layout.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return layout_Index; });
function layout__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { layout__typeof = function _typeof(obj) { return typeof obj; }; } else { layout__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return layout__typeof(obj); }

function layout__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function layout__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function layout__createClass(Constructor, protoProps, staticProps) { if (protoProps) layout__defineProperties(Constructor.prototype, protoProps); if (staticProps) layout__defineProperties(Constructor, staticProps); return Constructor; }

function layout__possibleConstructorReturn(self, call) { if (call && (layout__typeof(call) === "object" || typeof call === "function")) { return call; } return layout__assertThisInitialized(self); }

function layout__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function layout__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






fontawesome__default.a.library.add(fontawesome_free_solid_["faSearch"], fontawesome_free_solid_["faCartPlus"], fontawesome_free_solid_["faAngleDoubleRight"], fontawesome_free_solid_["faEllipsisH"], fontawesome_free_solid_["faHeart"], fontawesome_free_solid_["faPenSquare"], fontawesome_free_solid_["faCheckCircle"], fontawesome_free_solid_["faExclamationCircle"], fontawesome_free_solid_["faTrashAlt"]);

var layout_Index =
/*#__PURE__*/
function (_Component) {
  layout__inherits(Index, _Component);

  function Index() {
    layout__classCallCheck(this, Index);

    return layout__possibleConstructorReturn(this, (Index.__proto__ || Object.getPrototypeOf(Index)).apply(this, arguments));
  }

  layout__createClass(Index, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", null, external__react__default.a.createElement(head__default.a, null, external__react__default.a.createElement("link", {
        href: "/css/core.css",
        rel: "stylesheet"
      }), external__react__default.a.createElement("link", {
        href: "/css/style.css",
        rel: "stylesheet"
      })), external__react__default.a.createElement(header_Header, null), external__react__default.a.createElement("div", {
        className: "container mc"
      }, this.props.children), external__react__default.a.createElement("script", {
        src: "/js/core.js"
      }));
    }
  }]);

  return Index;
}(external__react_["Component"]);



/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/fontawesome-free-solid");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("react-load-image");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),
/* 11 */,
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Product; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_link__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_link___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_next_link__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_load_image__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_load_image___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_load_image__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fortawesome_react_fontawesome__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fortawesome_react_fontawesome___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__fortawesome_react_fontawesome__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var Product =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Product, _React$Component);

  function Product() {
    _classCallCheck(this, Product);

    return _possibleConstructorReturn(this, (Product.__proto__ || Object.getPrototypeOf(Product)).apply(this, arguments));
  }

  _createClass(Product, [{
    key: "render",
    value: function render() {
      var detailsURL = "/d/" + this.props.product.name.replace(/\s/g, "-") + "/" + this.props.product.id;
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: this.props.columnClass
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: "card box-shadow"
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_react_load_image___default.a, {
        src: this.props.product.images.preview
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("img", {
        className: "card-img-top"
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", null, "Error!"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("img", {
        src: "/img/ajax.gif"
      })), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: "card-body"
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("span", {
        className: "badge badge-success"
      }, "$", this.props.product.price), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("p", {
        className: "card-text"
      }, this.props.product.name), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
        href: "#",
        className: "btn btn-sm btn-outline-secondary btn-product"
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("span", null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__fortawesome_react_fontawesome___default.a, {
        icon: "cart-plus"
      }), " Add to cart")), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_link___default.a, {
        href: {
          pathname: "detail",
          query: {
            name: this.props.product.name.replace(/\s/g, "-"),
            id: this.props.product.id
          }
        },
        as: detailsURL
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
        className: "btn btn-sm btn-success btn-product"
      }, "View Details")), this.props.children)));
    }
  }]);

  return Product;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);



/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PageTooltip; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_reactstrap__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_reactstrap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_reactstrap__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }




var PageTooltip =
/*#__PURE__*/
function (_Component) {
  _inherits(PageTooltip, _Component);

  function PageTooltip(props) {
    var _this;

    _classCallCheck(this, PageTooltip);

    _this = _possibleConstructorReturn(this, (PageTooltip.__proto__ || Object.getPrototypeOf(PageTooltip)).call(this, props));
    _this.toggle = _this.toggle.bind(_assertThisInitialized(_this));
    _this.state = {
      tooltipOpen: false
    };
    return _this;
  }

  _createClass(PageTooltip, [{
    key: "toggle",
    value: function toggle() {
      this.setState({
        tooltipOpen: !this.state.tooltipOpen
      });
    }
  }, {
    key: "render",
    value: function render() {
      var placement = this.props.position ? this.props.position : 'top';
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_reactstrap__["Tooltip"], {
        placement: placement,
        isOpen: this.state.tooltipOpen,
        target: this.props.target,
        toggle: this.toggle
      }, this.props.message);
    }
  }]);

  return PageTooltip;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);



/***/ }),
/* 14 */,
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "next/link"
var link_ = __webpack_require__(10);
var link__default = /*#__PURE__*/__webpack_require__.n(link_);

// EXTERNAL MODULE: external "reactstrap"
var external__reactstrap_ = __webpack_require__(2);
var external__reactstrap__default = /*#__PURE__*/__webpack_require__.n(external__reactstrap_);

// EXTERNAL MODULE: external "react-load-image"
var external__react_load_image_ = __webpack_require__(9);
var external__react_load_image__default = /*#__PURE__*/__webpack_require__.n(external__react_load_image_);

// EXTERNAL MODULE: ./components/misc/tooltip.js
var tooltip = __webpack_require__(13);

// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(1);
var react_fontawesome__default = /*#__PURE__*/__webpack_require__.n(react_fontawesome_);

// CONCATENATED MODULE: ./components/misc/verified.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var verified_Verified =
/*#__PURE__*/
function (_Component) {
  _inherits(Verified, _Component);

  function Verified(props) {
    var _this;

    _classCallCheck(this, Verified);

    _this = _possibleConstructorReturn(this, (Verified.__proto__ || Object.getPrototypeOf(Verified)).call(this, props));
    _this.state = {
      message: props.message || 'Verified',
      tooltipPosition: props.position || 'top'
    };
    return _this;
  }

  _createClass(Verified, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        className: "verified",
        id: "verified"
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "check-circle"
      }), external__react__default.a.createElement(tooltip["a" /* default */], {
        position: this.state.tooltipPosition,
        target: "verified",
        message: this.state.message
      }));
    }
  }]);

  return Verified;
}(external__react_["Component"]);


// CONCATENATED MODULE: ./components/accounts/settings.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return settings_Layout; });
function settings__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { settings__typeof = function _typeof(obj) { return typeof obj; }; } else { settings__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return settings__typeof(obj); }

function settings__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function settings__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function settings__createClass(Constructor, protoProps, staticProps) { if (protoProps) settings__defineProperties(Constructor.prototype, protoProps); if (staticProps) settings__defineProperties(Constructor, staticProps); return Constructor; }

function settings__possibleConstructorReturn(self, call) { if (call && (settings__typeof(call) === "object" || typeof call === "function")) { return call; } return settings__assertThisInitialized(self); }

function settings__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function settings__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }







var settings_Layout =
/*#__PURE__*/
function (_Component) {
  settings__inherits(Layout, _Component);

  function Layout(props) {
    var _this;

    settings__classCallCheck(this, Layout);

    _this = settings__possibleConstructorReturn(this, (Layout.__proto__ || Object.getPrototypeOf(Layout)).call(this, props));
    _this.toggle = _this.toggle.bind(settings__assertThisInitialized(_this));
    _this.state = {
      activeTab: "1"
    };
    return _this;
  }

  settings__createClass(Layout, [{
    key: "toggle",
    value: function toggle(tab) {
      if (this.state.activeTab !== tab) {
        this.setState({
          activeTab: tab
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        id: "account"
      }, external__react__default.a.createElement("div", {
        className: "page-banner"
      }, external__react__default.a.createElement("div", {
        className: "container mc"
      }, external__react__default.a.createElement(external__reactstrap_["Col"], {
        lg: "12",
        md: "12",
        sm: "12"
      }, external__react__default.a.createElement("span", null, "Settings")))), external__react__default.a.createElement(external__reactstrap_["Row"], null, external__react__default.a.createElement(external__reactstrap_["Col"], {
        lg: "4",
        md: "4",
        sm: "12",
        xs: "12"
      }, external__react__default.a.createElement(external__reactstrap_["Card"], {
        className: "user-profile"
      }, external__react__default.a.createElement("div", {
        className: "user-header"
      }, external__react__default.a.createElement("div", {
        className: "user-header-wrapper"
      }, external__react__default.a.createElement("div", {
        className: "user-avatar"
      }, external__react__default.a.createElement(external__react_load_image__default.a, {
        src: "http://via.placeholder.com/200x200/000/fff/"
      }, external__react__default.a.createElement("img", {
        className: "user-profile-pic rounded-circle"
      }), external__react__default.a.createElement("div", null, "Error!"), external__react__default.a.createElement("img", {
        src: "/img/ajax.gif"
      }))), external__react__default.a.createElement("div", {
        className: "user-identity"
      }, external__react__default.a.createElement("h5", null, "Peter", external__react__default.a.createElement(verified_Verified, {
        position: "right"
      })), external__react__default.a.createElement("span", null, "Joined Dec 12, 2012")))), external__react__default.a.createElement(external__reactstrap_["CardBody"], null, external__react__default.a.createElement(external__reactstrap_["ListGroup"], null, external__react__default.a.createElement(external__reactstrap_["ListGroupItem"], null, external__react__default.a.createElement(link__default.a, {
        href: "/settings/user"
      }, external__react__default.a.createElement("a", null, "Users"))), external__react__default.a.createElement(external__reactstrap_["ListGroupItem"], null, external__react__default.a.createElement(link__default.a, {
        href: "/settings/profile"
      }, external__react__default.a.createElement("a", null, "My Profile"))), external__react__default.a.createElement(external__reactstrap_["ListGroupItem"], null, external__react__default.a.createElement(link__default.a, {
        href: "/settings/changepassword"
      }, external__react__default.a.createElement("a", null, "Change Password"))))))), external__react__default.a.createElement(external__reactstrap_["Col"], null, this.props.children)));
    }
  }]);

  return Layout;
}(external__react_["Component"]);



/***/ }),
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(29);


/***/ }),
/* 29 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(3);
var head__default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: ./components/layout.js + 1 modules
var layout = __webpack_require__(7);

// EXTERNAL MODULE: ./components/accounts/settings.js + 1 modules
var settings = __webpack_require__(15);

// EXTERNAL MODULE: ./components/product/product.js
var product = __webpack_require__(12);

// CONCATENATED MODULE: ./components/accounts/settings.profile.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var settings_profile_SettingsProfile =
/*#__PURE__*/
function (_Component) {
  _inherits(SettingsProfile, _Component);

  function SettingsProfile() {
    _classCallCheck(this, SettingsProfile);

    return _possibleConstructorReturn(this, (SettingsProfile.__proto__ || Object.getPrototypeOf(SettingsProfile)).apply(this, arguments));
  }

  _createClass(SettingsProfile, [{
    key: "render",
    value: function render() {
      var item = {
        id: 1,
        name: "item 1",
        description: "description for item 2",
        price: 101.7,
        images: {
          original: ["http://via.placeholder.com/280x365/000/fff/", "http://via.placeholder.com/280x365/000/fff/", "http://via.placeholder.com/280x365/000/fff/"],
          preview: "http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg",
          thumbnails: ["http://via.placeholder.com/90x70/000/fff/", "http://via.placeholder.com/90x70/000/fff/", "http://via.placeholder.com/90x70/000/fff/"]
        }
      };
      return external__react__default.a.createElement("div", null, external__react__default.a.createElement(head__default.a, null, external__react__default.a.createElement("link", {
        rel: "stylesheet",
        href: "/css/posting.css"
      })), external__react__default.a.createElement("div", {
        className: "posting"
      }, external__react__default.a.createElement("h4", null, "Product Listing"), external__react__default.a.createElement("hr", null), external__react__default.a.createElement("form", null, external__react__default.a.createElement("div", {
        className: "form-group"
      }, external__react__default.a.createElement("label", null, "Product Name"), external__react__default.a.createElement("input", {
        type: "text",
        className: "form-control",
        placeholder: "Name"
      })), external__react__default.a.createElement("div", {
        className: "form-group"
      }, external__react__default.a.createElement("label", null, "Product Price"), external__react__default.a.createElement("input", {
        type: "text",
        className: "form-control",
        placeholder: "Price"
      })), external__react__default.a.createElement("div", {
        className: "text-right"
      }, external__react__default.a.createElement("input", {
        type: "submit",
        className: "btn btn-primary",
        value: "Post"
      })))), external__react__default.a.createElement("div", {
        className: "row"
      }, external__react__default.a.createElement(product["a" /* default */], {
        product: item,
        columnClass: "col-xs-12 col-sm-12 col-md-4 col-lg-4"
      }, external__react__default.a.createElement("a", {
        href: "#",
        className: "btn btn-sm btn-danger btn-product"
      }, "Delete"))));
    }
  }]);

  return SettingsProfile;
}(external__react_["Component"]);


// CONCATENATED MODULE: ./pages/settings-profile.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return settings_profile_Settings; });
function settings_profile__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { settings_profile__typeof = function _typeof(obj) { return typeof obj; }; } else { settings_profile__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return settings_profile__typeof(obj); }

function settings_profile__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function settings_profile__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function settings_profile__createClass(Constructor, protoProps, staticProps) { if (protoProps) settings_profile__defineProperties(Constructor.prototype, protoProps); if (staticProps) settings_profile__defineProperties(Constructor, staticProps); return Constructor; }

function settings_profile__possibleConstructorReturn(self, call) { if (call && (settings_profile__typeof(call) === "object" || typeof call === "function")) { return call; } return settings_profile__assertThisInitialized(self); }

function settings_profile__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function settings_profile__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }







var settings_profile_Settings =
/*#__PURE__*/
function (_Component) {
  settings_profile__inherits(Settings, _Component);

  function Settings() {
    settings_profile__classCallCheck(this, Settings);

    return settings_profile__possibleConstructorReturn(this, (Settings.__proto__ || Object.getPrototypeOf(Settings)).apply(this, arguments));
  }

  settings_profile__createClass(Settings, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement(layout["a" /* default */], null, external__react__default.a.createElement(head__default.a, null, external__react__default.a.createElement("link", {
        rel: "stylesheet",
        href: "/css/settings.css"
      }), external__react__default.a.createElement("link", {
        rel: "stylesheet",
        href: "/css/accounts.css"
      })), external__react__default.a.createElement(settings["a" /* default */], null, external__react__default.a.createElement(settings_profile_SettingsProfile, null)));
    }
  }]);

  return Settings;
}(external__react_["Component"]);



/***/ })
/******/ ]);