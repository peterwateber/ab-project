module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 21);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/react-fontawesome");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("reactstrap");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = {
  port: 3001,
  TIMEOUT: 30000,
  COOKIE: {
    loginCookie: 'project_cookie',
    loginSession: 'project_session',
    config: {
      maxAge: 60 * 60 * 24 * 365,
      httpOnly: true,
      // http only, prevents JavaScript cookie access
      secure: false // cookie must be sent over https / ssl

    }
  }
};

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/fontawesome");

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(3);
var head__default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: external "reactstrap"
var external__reactstrap_ = __webpack_require__(2);
var external__reactstrap__default = /*#__PURE__*/__webpack_require__.n(external__reactstrap_);

// EXTERNAL MODULE: external "axios"
var external__axios_ = __webpack_require__(4);
var external__axios__default = /*#__PURE__*/__webpack_require__.n(external__axios_);

// EXTERNAL MODULE: ./constant/config.js
var config = __webpack_require__(5);
var config_default = /*#__PURE__*/__webpack_require__.n(config);

// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(1);
var react_fontawesome__default = /*#__PURE__*/__webpack_require__.n(react_fontawesome_);

// CONCATENATED MODULE: ./components/header.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }







var header_Header =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Header, _React$Component);

  function Header() {
    var _this;

    _classCallCheck(this, Header);

    _this = _possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).call(this));
    _this.toggle = _this.toggle.bind(_assertThisInitialized(_this));
    _this.state = {
      name: 'Login',
      islogin: false,
      user_url: '/login',
      dropdownOpen: false
    };
    return _this;
  }

  _createClass(Header, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var headerXhr = external__axios__default.a.create();
      headerXhr.defaults.timeout = config_default.a.TIMEOUT;
      external__axios__default.a.post('/ajax', {
        action: 'header'
      }).then(function (result) {
        return result.data;
      }).then(function (data) {
        if (data.success) {
          _this2.setState({
            name: data.result.firstname,
            islogin: data.success,
            user_url: '#'
          });
        }
      }).catch(function (err) {});
    }
  }, {
    key: "toggle",
    value: function toggle() {
      this.setState({
        dropdownOpen: !this.state.dropdownOpen
      });
    }
  }, {
    key: "render",
    value: function render() {
      var hideNavLinks = false;

      if (this.props.hideNavLinks === 'true') {
        hideNavLinks = true;
      }

      return external__react__default.a.createElement("nav", {
        className: "navbar navbar-expand-lg navbar-light bg-light"
      }, external__react__default.a.createElement("div", {
        className: "container"
      }, external__react__default.a.createElement("a", {
        href: "/",
        className: "navbar-brand"
      }, "Project"), external__react__default.a.createElement("button", {
        className: "navbar-toggler",
        type: "button",
        "data-toggle": "collapse",
        "data-target": "#navbar",
        "aria-controls": "navbarsExample07",
        "aria-expanded": "false",
        "aria-label": "Toggle navigation"
      }, external__react__default.a.createElement("span", {
        className: "navbar-toggler-icon"
      })), external__react__default.a.createElement("div", {
        className: "collapse navbar-collapse",
        id: "navbar"
      }, hideNavLinks === false && external__react__default.a.createElement("ul", {
        className: "navbar-nav mr-auto"
      }, external__react__default.a.createElement("li", {
        className: "nav-item"
      }, external__react__default.a.createElement("a", {
        className: "nav-link",
        href: "#"
      }, "Inactive Link")), external__react__default.a.createElement("li", {
        className: "nav-item dropdown"
      }, external__react__default.a.createElement("a", {
        href: "#",
        className: "nav-link dropdown-toggle",
        id: "dropdown",
        "aria-haspopup": "true",
        "aria-expanded": "false",
        "data-toggle": "dropdown"
      }, "Dropdown"), external__react__default.a.createElement("div", {
        className: "dropdown-menu",
        "aria-labelledby": "dropdown"
      }, external__react__default.a.createElement("a", {
        className: "dropdown-item",
        href: "#"
      }, "Action"), external__react__default.a.createElement("a", {
        className: "dropdown-item",
        href: "#"
      }, "Another action"), external__react__default.a.createElement("a", {
        className: "dropdown-item",
        href: "#"
      }, "Something else here")))), external__react__default.a.createElement("form", {
        className: "form-inline ml-auto form-navigation"
      }, external__react__default.a.createElement("div", {
        className: "form-navigation-wrapper"
      }, external__react__default.a.createElement("span", {
        className: "fa search-area"
      }, external__react__default.a.createElement("span", {
        className: "search-icon"
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "search"
      })), external__react__default.a.createElement("span", {
        className: "form-input-holder"
      }, external__react__default.a.createElement("input", {
        className: "form-control",
        type: "text",
        placeholder: "Search",
        "aria-label": "Search"
      }))), external__react__default.a.createElement(external__reactstrap_["ButtonDropdown"], {
        className: "fa",
        isOpen: this.state.dropdownOpen,
        toggle: this.toggle
      }, !this.state.islogin && external__react__default.a.createElement(external__reactstrap_["DropdownToggle"], {
        href: this.state.user_url
      }, this.state.name), this.state.islogin && external__react__default.a.createElement(external__reactstrap_["DropdownToggle"], {
        caret: true
      }, this.state.name), this.state.islogin && external__react__default.a.createElement(external__reactstrap_["DropdownMenu"], null, external__react__default.a.createElement(external__reactstrap_["DropdownItem"], {
        href: "/settings"
      }, "My account"), external__react__default.a.createElement(external__reactstrap_["DropdownItem"], {
        href: "/logout"
      }, "Logout"))), external__react__default.a.createElement("a", {
        href: "/details",
        className: "fa"
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "cart-plus"
      })))))));
    }
  }]);

  return Header;
}(external__react__default.a.Component);


// EXTERNAL MODULE: external "@fortawesome/fontawesome"
var fontawesome_ = __webpack_require__(6);
var fontawesome__default = /*#__PURE__*/__webpack_require__.n(fontawesome_);

// EXTERNAL MODULE: external "@fortawesome/fontawesome-free-solid"
var fontawesome_free_solid_ = __webpack_require__(8);
var fontawesome_free_solid__default = /*#__PURE__*/__webpack_require__.n(fontawesome_free_solid_);

// CONCATENATED MODULE: ./components/layout.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return layout_Index; });
function layout__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { layout__typeof = function _typeof(obj) { return typeof obj; }; } else { layout__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return layout__typeof(obj); }

function layout__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function layout__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function layout__createClass(Constructor, protoProps, staticProps) { if (protoProps) layout__defineProperties(Constructor.prototype, protoProps); if (staticProps) layout__defineProperties(Constructor, staticProps); return Constructor; }

function layout__possibleConstructorReturn(self, call) { if (call && (layout__typeof(call) === "object" || typeof call === "function")) { return call; } return layout__assertThisInitialized(self); }

function layout__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function layout__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






fontawesome__default.a.library.add(fontawesome_free_solid_["faSearch"], fontawesome_free_solid_["faCartPlus"], fontawesome_free_solid_["faAngleDoubleRight"], fontawesome_free_solid_["faEllipsisH"], fontawesome_free_solid_["faHeart"], fontawesome_free_solid_["faPenSquare"], fontawesome_free_solid_["faCheckCircle"], fontawesome_free_solid_["faExclamationCircle"], fontawesome_free_solid_["faTrashAlt"]);

var layout_Index =
/*#__PURE__*/
function (_Component) {
  layout__inherits(Index, _Component);

  function Index() {
    layout__classCallCheck(this, Index);

    return layout__possibleConstructorReturn(this, (Index.__proto__ || Object.getPrototypeOf(Index)).apply(this, arguments));
  }

  layout__createClass(Index, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", null, external__react__default.a.createElement(head__default.a, null, external__react__default.a.createElement("link", {
        href: "/css/core.css",
        rel: "stylesheet"
      }), external__react__default.a.createElement("link", {
        href: "/css/style.css",
        rel: "stylesheet"
      })), external__react__default.a.createElement(header_Header, null), external__react__default.a.createElement("div", {
        className: "container mc"
      }, this.props.children), external__react__default.a.createElement("script", {
        src: "/js/core.js"
      }));
    }
  }]);

  return Index;
}(external__react_["Component"]);



/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/fontawesome-free-solid");

/***/ }),
/* 9 */,
/* 10 */,
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(1);
var react_fontawesome__default = /*#__PURE__*/__webpack_require__.n(react_fontawesome_);

// CONCATENATED MODULE: ./libs/form.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }



var form_Form =
/*#__PURE__*/
function (_Component) {
  _inherits(Form, _Component);

  function Form(props) {
    var _this;

    _classCallCheck(this, Form);

    _this = _possibleConstructorReturn(this, (Form.__proto__ || Object.getPrototypeOf(Form)).call(this, props));
    _this.state = {
      name: props.name,
      disabled: props.disabled || '',
      placeholder: props.placeholder || '',
      id: props.id || '',
      className: props.className || '',
      type: props.type || 'text',
      text: props.text || '',
      message: props.message || ''
    };
    return _this;
  }

  _createClass(Form, [{
    key: "render",
    value: function render() {
      var formClass = this.state.type === 'checkbox' ? '' : 'form-field ';
      var placeholderClass = this.props.value.length > 0 ? 'extended' : '';
      var inputfield = external__react__default.a.createElement("input", {
        type: this.state.type,
        id: this.state.id,
        className: this.state.type !== 'checkbox' ? 'form-control '.concat(this.state.className) : this.state.className,
        name: this.state.name,
        onChange: this.props.onChange,
        disabled: this.state.disabled,
        value: this.props.value
      });
      return external__react__default.a.createElement("div", {
        className: formClass.concat(this.state.message ? 'field-error' : '')
      }, external__react__default.a.createElement("div", {
        className: "form-group"
      }, external__react__default.a.createElement("label", {
        htmlFor: this.state.id,
        className: "placeholder ".concat(placeholderClass)
      }, this.state.placeholder), inputfield, " ", this.state.text || '', this.state.type !== 'checkbox' && external__react__default.a.createElement("div", {
        className: "placeholder-label"
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "exclamation-circle"
      }))), external__react__default.a.createElement("div", {
        className: "feedback"
      }, this.state.message));
    }
  }]);

  return Form;
}(external__react_["Component"]);
// CONCATENATED MODULE: ./libs/factories.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormInput; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return layoutGrid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return pagination; });

var FormInput = form_Form;
var layoutGrid = function layoutGrid(products) {
  var columns = [];

  try {
    var cols,
        totalColumn = products.length,
        chunk = 4;

    for (cols = 0; cols < totalColumn; cols += chunk) {
      columns.push(products.slice(cols, cols + chunk));
    }
  } catch (ex) {}

  return columns;
};
var pagination = function pagination(curr, lst) {
  var current = curr,
      last = lst,
      delta = 2,
      left = current - delta,
      right = current + delta + 1,
      range = [],
      rangeWithDots = [],
      l;

  for (var i = 1; i <= last; i++) {
    if (i == 1 || i == last || i >= left && i < right) {
      range.push(i);
    }
  }

  for (var _i2 = 0; _i2 < range.length; _i2++) {
    var _i = range[_i2];

    if (l) {
      if (_i - l === 2) {
        rangeWithDots.push(l + 1);
      } else if (_i - l !== 1) {
        rangeWithDots.push("...");
      }
    }

    rangeWithDots.push(_i);
    l = _i;
  }

  return rangeWithDots;
};

/***/ }),
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(22);


/***/ }),
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(3);
var head__default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: ./components/layout.js + 1 modules
var layout = __webpack_require__(7);

// EXTERNAL MODULE: external "react-dom"
var external__react_dom_ = __webpack_require__(23);
var external__react_dom__default = /*#__PURE__*/__webpack_require__.n(external__react_dom_);

// EXTERNAL MODULE: external "axios"
var external__axios_ = __webpack_require__(4);
var external__axios__default = /*#__PURE__*/__webpack_require__.n(external__axios_);

// EXTERNAL MODULE: ./libs/factories.js + 1 modules
var factories = __webpack_require__(11);

// CONCATENATED MODULE: ./components/accounts/login.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }






var login_Login =
/*#__PURE__*/
function (_Component) {
  _inherits(Login, _Component);

  function Login() {
    var _this;

    _classCallCheck(this, Login);

    _this = _possibleConstructorReturn(this, (Login.__proto__ || Object.getPrototypeOf(Login)).call(this));
    _this.onChange = _this.onChange.bind(_assertThisInitialized(_this));
    _this.handleSubmit = _this.handleSubmit.bind(_assertThisInitialized(_this));
    _this.state = {
      formData: {
        email: '',
        password: '',
        rem: ''
      },
      loading: false,
      error: {},
      message: '',
      action: 'login'
    };
    return _this;
  }

  _createClass(Login, [{
    key: "handleSubmit",
    value: function handleSubmit(e) {
      e.preventDefault();
      var self = this;
      self.setState({
        loading: true
      });
      var formData = Object.assign({
        action: self.state.action
      }, self.state.formData);
      external__axios__default.a.post('/ajax', formData).then(function (result) {
        return result.data;
      }).then(function (msg) {
        setTimeout(function () {
          if (msg.success) {
            window.location = msg.redirect;
          } else {
            self.setState({
              notificationClass: 'danger',
              loading: false,
              message: msg.message
            });
          }
        }, 200);
      }).catch(function (ex) {});
    }
  }, {
    key: "onChange",
    value: function onChange(e) {
      var state = this.state.formData;
      state[e.target.name] = e.target.value;

      if (e.target.type === 'checkbox') {
        state[e.target.name] = e.target.checked;
      }

      this.setState(state);
    }
  }, {
    key: "render",
    value: function render() {
      var loading = this.state.loading ? 'form-loading' : '';
      return external__react__default.a.createElement("form", {
        autoComplete: "off",
        onSubmit: this.handleSubmit,
        className: "form-signin"
      }, external__react__default.a.createElement("h1", {
        className: "h3 mb-3 font-weight-normal"
      }, "Please Login"), this.state.message && external__react__default.a.createElement("div", {
        className: "alert alert-danger"
      }, this.state.message), external__react__default.a.createElement(factories["a" /* FormInput */], {
        name: "email",
        id: "email",
        placeholder: "Email Address",
        onChange: this.onChange,
        disabled: this.state.loading,
        message: this.state.error.email,
        value: this.state.formData.email
      }), external__react__default.a.createElement(factories["a" /* FormInput */], {
        name: "password",
        id: "password",
        type: "password",
        placeholder: "Password",
        onChange: this.onChange,
        disabled: this.state.loading,
        message: this.state.error.password,
        value: this.state.formData.password
      }), external__react__default.a.createElement("div", {
        className: "checkbox mb-3"
      }, external__react__default.a.createElement("label", null, external__react__default.a.createElement(factories["a" /* FormInput */], {
        type: "checkbox",
        name: "rem",
        text: "Remember me",
        onChange: this.onChange,
        value: this.state.formData.rem
      }))), external__react__default.a.createElement("button", {
        disabled: this.state.loading,
        className: 'btn btn-lg btn-success btn-block btn-login '.concat(loading),
        type: "submit"
      }, "Login"), external__react__default.a.createElement("a", {
        href: "/register",
        className: "btn btn-lg btn-link btn-block btn-register"
      }, "No Account? Create here!"), external__react__default.a.createElement("p", {
        className: "mt-5 mb-3 text-muted"
      }, "\xA9 ", new Date().getFullYear()));
    }
  }]);

  return Login;
}(external__react_["Component"]);


// CONCATENATED MODULE: ./pages/login.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return pages_login_Login; });
function login__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { login__typeof = function _typeof(obj) { return typeof obj; }; } else { login__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return login__typeof(obj); }

function login__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function login__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function login__createClass(Constructor, protoProps, staticProps) { if (protoProps) login__defineProperties(Constructor.prototype, protoProps); if (staticProps) login__defineProperties(Constructor, staticProps); return Constructor; }

function login__possibleConstructorReturn(self, call) { if (call && (login__typeof(call) === "object" || typeof call === "function")) { return call; } return login__assertThisInitialized(self); }

function login__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function login__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var pages_login_Login =
/*#__PURE__*/
function (_Component) {
  login__inherits(Login, _Component);

  function Login() {
    login__classCallCheck(this, Login);

    return login__possibleConstructorReturn(this, (Login.__proto__ || Object.getPrototypeOf(Login)).apply(this, arguments));
  }

  login__createClass(Login, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement(layout["a" /* default */], null, external__react__default.a.createElement(head__default.a, null, external__react__default.a.createElement("link", {
        href: "/css/accounts.css",
        rel: "stylesheet"
      })), external__react__default.a.createElement("div", {
        id: "account"
      }, external__react__default.a.createElement(login_Login, null)));
    }
  }]);

  return Login;
}(external__react_["Component"]);



/***/ }),
/* 23 */
/***/ (function(module, exports) {

module.exports = require("react-dom");

/***/ })
/******/ ]);