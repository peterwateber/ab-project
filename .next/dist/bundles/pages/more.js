module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 24);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/react-fontawesome");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("reactstrap");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = {
  port: 3001,
  TIMEOUT: 30000,
  COOKIE: {
    loginCookie: 'project_cookie',
    loginSession: 'project_session',
    config: {
      maxAge: 60 * 60 * 24 * 365,
      httpOnly: true,
      // http only, prevents JavaScript cookie access
      secure: false // cookie must be sent over https / ssl

    }
  }
};

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/fontawesome");

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(3);
var head__default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: external "reactstrap"
var external__reactstrap_ = __webpack_require__(2);
var external__reactstrap__default = /*#__PURE__*/__webpack_require__.n(external__reactstrap_);

// EXTERNAL MODULE: external "axios"
var external__axios_ = __webpack_require__(4);
var external__axios__default = /*#__PURE__*/__webpack_require__.n(external__axios_);

// EXTERNAL MODULE: ./constant/config.js
var config = __webpack_require__(5);
var config_default = /*#__PURE__*/__webpack_require__.n(config);

// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(1);
var react_fontawesome__default = /*#__PURE__*/__webpack_require__.n(react_fontawesome_);

// CONCATENATED MODULE: ./components/header.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }







var header_Header =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Header, _React$Component);

  function Header() {
    var _this;

    _classCallCheck(this, Header);

    _this = _possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).call(this));
    _this.toggle = _this.toggle.bind(_assertThisInitialized(_this));
    _this.state = {
      name: 'Login',
      islogin: false,
      user_url: '/login',
      dropdownOpen: false
    };
    return _this;
  }

  _createClass(Header, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var headerXhr = external__axios__default.a.create();
      headerXhr.defaults.timeout = config_default.a.TIMEOUT;
      external__axios__default.a.post('/ajax', {
        action: 'header'
      }).then(function (result) {
        return result.data;
      }).then(function (data) {
        if (data.success) {
          _this2.setState({
            name: data.result.firstname,
            islogin: data.success,
            user_url: '#'
          });
        }
      }).catch(function (err) {});
    }
  }, {
    key: "toggle",
    value: function toggle() {
      this.setState({
        dropdownOpen: !this.state.dropdownOpen
      });
    }
  }, {
    key: "render",
    value: function render() {
      var hideNavLinks = false;

      if (this.props.hideNavLinks === 'true') {
        hideNavLinks = true;
      }

      return external__react__default.a.createElement("nav", {
        className: "navbar navbar-expand-lg navbar-light bg-light"
      }, external__react__default.a.createElement("div", {
        className: "container"
      }, external__react__default.a.createElement("a", {
        href: "/",
        className: "navbar-brand"
      }, "Project"), external__react__default.a.createElement("button", {
        className: "navbar-toggler",
        type: "button",
        "data-toggle": "collapse",
        "data-target": "#navbar",
        "aria-controls": "navbarsExample07",
        "aria-expanded": "false",
        "aria-label": "Toggle navigation"
      }, external__react__default.a.createElement("span", {
        className: "navbar-toggler-icon"
      })), external__react__default.a.createElement("div", {
        className: "collapse navbar-collapse",
        id: "navbar"
      }, hideNavLinks === false && external__react__default.a.createElement("ul", {
        className: "navbar-nav mr-auto"
      }, external__react__default.a.createElement("li", {
        className: "nav-item"
      }, external__react__default.a.createElement("a", {
        className: "nav-link",
        href: "#"
      }, "Inactive Link")), external__react__default.a.createElement("li", {
        className: "nav-item dropdown"
      }, external__react__default.a.createElement("a", {
        href: "#",
        className: "nav-link dropdown-toggle",
        id: "dropdown",
        "aria-haspopup": "true",
        "aria-expanded": "false",
        "data-toggle": "dropdown"
      }, "Dropdown"), external__react__default.a.createElement("div", {
        className: "dropdown-menu",
        "aria-labelledby": "dropdown"
      }, external__react__default.a.createElement("a", {
        className: "dropdown-item",
        href: "#"
      }, "Action"), external__react__default.a.createElement("a", {
        className: "dropdown-item",
        href: "#"
      }, "Another action"), external__react__default.a.createElement("a", {
        className: "dropdown-item",
        href: "#"
      }, "Something else here")))), external__react__default.a.createElement("form", {
        className: "form-inline ml-auto form-navigation"
      }, external__react__default.a.createElement("div", {
        className: "form-navigation-wrapper"
      }, external__react__default.a.createElement("span", {
        className: "fa search-area"
      }, external__react__default.a.createElement("span", {
        className: "search-icon"
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "search"
      })), external__react__default.a.createElement("span", {
        className: "form-input-holder"
      }, external__react__default.a.createElement("input", {
        className: "form-control",
        type: "text",
        placeholder: "Search",
        "aria-label": "Search"
      }))), external__react__default.a.createElement(external__reactstrap_["ButtonDropdown"], {
        className: "fa",
        isOpen: this.state.dropdownOpen,
        toggle: this.toggle
      }, !this.state.islogin && external__react__default.a.createElement(external__reactstrap_["DropdownToggle"], {
        href: this.state.user_url
      }, this.state.name), this.state.islogin && external__react__default.a.createElement(external__reactstrap_["DropdownToggle"], {
        caret: true
      }, this.state.name), this.state.islogin && external__react__default.a.createElement(external__reactstrap_["DropdownMenu"], null, external__react__default.a.createElement(external__reactstrap_["DropdownItem"], {
        href: "/settings"
      }, "My account"), external__react__default.a.createElement(external__reactstrap_["DropdownItem"], {
        href: "/logout"
      }, "Logout"))), external__react__default.a.createElement("a", {
        href: "/details",
        className: "fa"
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "cart-plus"
      })))))));
    }
  }]);

  return Header;
}(external__react__default.a.Component);


// EXTERNAL MODULE: external "@fortawesome/fontawesome"
var fontawesome_ = __webpack_require__(6);
var fontawesome__default = /*#__PURE__*/__webpack_require__.n(fontawesome_);

// EXTERNAL MODULE: external "@fortawesome/fontawesome-free-solid"
var fontawesome_free_solid_ = __webpack_require__(8);
var fontawesome_free_solid__default = /*#__PURE__*/__webpack_require__.n(fontawesome_free_solid_);

// CONCATENATED MODULE: ./components/layout.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return layout_Index; });
function layout__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { layout__typeof = function _typeof(obj) { return typeof obj; }; } else { layout__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return layout__typeof(obj); }

function layout__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function layout__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function layout__createClass(Constructor, protoProps, staticProps) { if (protoProps) layout__defineProperties(Constructor.prototype, protoProps); if (staticProps) layout__defineProperties(Constructor, staticProps); return Constructor; }

function layout__possibleConstructorReturn(self, call) { if (call && (layout__typeof(call) === "object" || typeof call === "function")) { return call; } return layout__assertThisInitialized(self); }

function layout__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function layout__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






fontawesome__default.a.library.add(fontawesome_free_solid_["faSearch"], fontawesome_free_solid_["faCartPlus"], fontawesome_free_solid_["faAngleDoubleRight"], fontawesome_free_solid_["faEllipsisH"], fontawesome_free_solid_["faHeart"], fontawesome_free_solid_["faPenSquare"], fontawesome_free_solid_["faCheckCircle"], fontawesome_free_solid_["faExclamationCircle"], fontawesome_free_solid_["faTrashAlt"]);

var layout_Index =
/*#__PURE__*/
function (_Component) {
  layout__inherits(Index, _Component);

  function Index() {
    layout__classCallCheck(this, Index);

    return layout__possibleConstructorReturn(this, (Index.__proto__ || Object.getPrototypeOf(Index)).apply(this, arguments));
  }

  layout__createClass(Index, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", null, external__react__default.a.createElement(head__default.a, null, external__react__default.a.createElement("link", {
        href: "/css/core.css",
        rel: "stylesheet"
      }), external__react__default.a.createElement("link", {
        href: "/css/style.css",
        rel: "stylesheet"
      })), external__react__default.a.createElement(header_Header, null), external__react__default.a.createElement("div", {
        className: "container mc"
      }, this.props.children), external__react__default.a.createElement("script", {
        src: "/js/core.js"
      }));
    }
  }]);

  return Index;
}(external__react_["Component"]);



/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/fontawesome-free-solid");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("react-load-image");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(1);
var react_fontawesome__default = /*#__PURE__*/__webpack_require__.n(react_fontawesome_);

// CONCATENATED MODULE: ./libs/form.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }



var form_Form =
/*#__PURE__*/
function (_Component) {
  _inherits(Form, _Component);

  function Form(props) {
    var _this;

    _classCallCheck(this, Form);

    _this = _possibleConstructorReturn(this, (Form.__proto__ || Object.getPrototypeOf(Form)).call(this, props));
    _this.state = {
      name: props.name,
      disabled: props.disabled || '',
      placeholder: props.placeholder || '',
      id: props.id || '',
      className: props.className || '',
      type: props.type || 'text',
      text: props.text || '',
      message: props.message || ''
    };
    return _this;
  }

  _createClass(Form, [{
    key: "render",
    value: function render() {
      var formClass = this.state.type === 'checkbox' ? '' : 'form-field ';
      var placeholderClass = this.props.value.length > 0 ? 'extended' : '';
      var inputfield = external__react__default.a.createElement("input", {
        type: this.state.type,
        id: this.state.id,
        className: this.state.type !== 'checkbox' ? 'form-control '.concat(this.state.className) : this.state.className,
        name: this.state.name,
        onChange: this.props.onChange,
        disabled: this.state.disabled,
        value: this.props.value
      });
      return external__react__default.a.createElement("div", {
        className: formClass.concat(this.state.message ? 'field-error' : '')
      }, external__react__default.a.createElement("div", {
        className: "form-group"
      }, external__react__default.a.createElement("label", {
        htmlFor: this.state.id,
        className: "placeholder ".concat(placeholderClass)
      }, this.state.placeholder), inputfield, " ", this.state.text || '', this.state.type !== 'checkbox' && external__react__default.a.createElement("div", {
        className: "placeholder-label"
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "exclamation-circle"
      }))), external__react__default.a.createElement("div", {
        className: "feedback"
      }, this.state.message));
    }
  }]);

  return Form;
}(external__react_["Component"]);
// CONCATENATED MODULE: ./libs/factories.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormInput; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return layoutGrid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return pagination; });

var FormInput = form_Form;
var layoutGrid = function layoutGrid(products) {
  var columns = [];

  try {
    var cols,
        totalColumn = products.length,
        chunk = 4;

    for (cols = 0; cols < totalColumn; cols += chunk) {
      columns.push(products.slice(cols, cols + chunk));
    }
  } catch (ex) {}

  return columns;
};
var pagination = function pagination(curr, lst) {
  var current = curr,
      last = lst,
      delta = 2,
      left = current - delta,
      right = current + delta + 1,
      range = [],
      rangeWithDots = [],
      l;

  for (var i = 1; i <= last; i++) {
    if (i == 1 || i == last || i >= left && i < right) {
      range.push(i);
    }
  }

  for (var _i2 = 0; _i2 < range.length; _i2++) {
    var _i = range[_i2];

    if (l) {
      if (_i - l === 2) {
        rangeWithDots.push(l + 1);
      } else if (_i - l !== 1) {
        rangeWithDots.push("...");
      }
    }

    rangeWithDots.push(_i);
    l = _i;
  }

  return rangeWithDots;
};

/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Product; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_link__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_link___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_next_link__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_load_image__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_load_image___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_load_image__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fortawesome_react_fontawesome__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fortawesome_react_fontawesome___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__fortawesome_react_fontawesome__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var Product =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Product, _React$Component);

  function Product() {
    _classCallCheck(this, Product);

    return _possibleConstructorReturn(this, (Product.__proto__ || Object.getPrototypeOf(Product)).apply(this, arguments));
  }

  _createClass(Product, [{
    key: "render",
    value: function render() {
      var detailsURL = "/d/" + this.props.product.name.replace(/\s/g, "-") + "/" + this.props.product.id;
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: this.props.columnClass
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: "card box-shadow"
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_react_load_image___default.a, {
        src: this.props.product.images.preview
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("img", {
        className: "card-img-top"
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", null, "Error!"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("img", {
        src: "/img/ajax.gif"
      })), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: "card-body"
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("span", {
        className: "badge badge-success"
      }, "$", this.props.product.price), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("p", {
        className: "card-text"
      }, this.props.product.name), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
        href: "#",
        className: "btn btn-sm btn-outline-secondary btn-product"
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("span", null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__fortawesome_react_fontawesome___default.a, {
        icon: "cart-plus"
      }), " Add to cart")), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_link___default.a, {
        href: {
          pathname: "detail",
          query: {
            name: this.props.product.name.replace(/\s/g, "-"),
            id: this.props.product.id
          }
        },
        as: detailsURL
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
        className: "btn btn-sm btn-success btn-product"
      }, "View Details")), this.props.children)));
    }
  }]);

  return Product;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);



/***/ }),
/* 13 */,
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

var port = __webpack_require__(5).port;

var BASE_URL = 'http://localhost:' + port;
module.exports = {
  Headers: {},
  Accounts: {
    header: BASE_URL + '/api/user',
    displayName: BASE_URL + '/api/user',
    register: BASE_URL + '/api/register',
    login: BASE_URL + '/api/login'
  },
  Homepage: {
    home: BASE_URL + '/api/products'
  },
  Details: BASE_URL + '/api/product'
};

/***/ }),
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(25);


/***/ }),
/* 25 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(3);
var head__default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: ./components/layout.js + 1 modules
var layout = __webpack_require__(7);

// EXTERNAL MODULE: external "axios"
var external__axios_ = __webpack_require__(4);
var external__axios__default = /*#__PURE__*/__webpack_require__.n(external__axios_);

// EXTERNAL MODULE: ./constant/urls.js
var urls = __webpack_require__(14);
var urls_default = /*#__PURE__*/__webpack_require__.n(urls);

// EXTERNAL MODULE: ./libs/factories.js + 1 modules
var factories = __webpack_require__(11);

// EXTERNAL MODULE: ./components/product/product.js
var product = __webpack_require__(12);

// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(1);
var react_fontawesome__default = /*#__PURE__*/__webpack_require__.n(react_fontawesome_);

// CONCATENATED MODULE: ./components/load-more/loadmore.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var loadmore_LoadMore =
/*#__PURE__*/
function (_Component) {
  _inherits(LoadMore, _Component);

  function LoadMore() {
    _classCallCheck(this, LoadMore);

    return _possibleConstructorReturn(this, (LoadMore.__proto__ || Object.getPrototypeOf(LoadMore)).apply(this, arguments));
  }

  _createClass(LoadMore, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        className: "row"
      }, external__react__default.a.createElement("div", {
        className: "col-xs-12 col-sm-12 col-md-12 col-lg-12 load-more text-center"
      }, external__react__default.a.createElement(react_fontawesome__default.a, {
        icon: "ellipsis-h"
      })));
    }
  }]);

  return LoadMore;
}(external__react_["Component"]);


// CONCATENATED MODULE: ./components/product-filter/filter.js
function filter__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { filter__typeof = function _typeof(obj) { return typeof obj; }; } else { filter__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return filter__typeof(obj); }

function filter__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function filter__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function filter__createClass(Constructor, protoProps, staticProps) { if (protoProps) filter__defineProperties(Constructor.prototype, protoProps); if (staticProps) filter__defineProperties(Constructor, staticProps); return Constructor; }

function filter__possibleConstructorReturn(self, call) { if (call && (filter__typeof(call) === "object" || typeof call === "function")) { return call; } return filter__assertThisInitialized(self); }

function filter__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function filter__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }



var filter_Filter =
/*#__PURE__*/
function (_Component) {
  filter__inherits(Filter, _Component);

  function Filter() {
    filter__classCallCheck(this, Filter);

    return filter__possibleConstructorReturn(this, (Filter.__proto__ || Object.getPrototypeOf(Filter)).apply(this, arguments));
  }

  filter__createClass(Filter, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("ul", {
        className: "list-group"
      }, external__react__default.a.createElement("li", {
        className: "list-group-item list-group-header"
      }, this.props.category));
    }
  }]);

  return Filter;
}(external__react_["Component"]);


// CONCATENATED MODULE: ./components/product-list/list.js
function list__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { list__typeof = function _typeof(obj) { return typeof obj; }; } else { list__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return list__typeof(obj); }

function list__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function list__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function list__createClass(Constructor, protoProps, staticProps) { if (protoProps) list__defineProperties(Constructor.prototype, protoProps); if (staticProps) list__defineProperties(Constructor, staticProps); return Constructor; }

function list__possibleConstructorReturn(self, call) { if (call && (list__typeof(call) === "object" || typeof call === "function")) { return call; } return list__assertThisInitialized(self); }

function list__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function list__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }









var list_Lists =
/*#__PURE__*/
function (_Component) {
  list__inherits(Lists, _Component);

  function Lists(props) {
    var _this;

    list__classCallCheck(this, Lists);

    _this = list__possibleConstructorReturn(this, (Lists.__proto__ || Object.getPrototypeOf(Lists)).call(this, props));
    _this.renderProducts = _this.renderProducts.bind(list__assertThisInitialized(_this));
    _this.state = {
      groupName: props.groupName.replace(/\+/g, ' ') || '',
      products: {},
      fetching: true
    };
    return _this;
  }

  list__createClass(Lists, [{
    key: "fetchHomepageProducts",
    value: function fetchHomepageProducts() {
      var _this2 = this;

      external__axios__default.a.get(urls_default.a.Homepage.home).then(function (response) {
        return response.data.result;
      }).then(function (response) {
        _this2.setState({
          fetching: false,
          products: response.success
        });
      }).catch(function (ex) {
        console.log(ex);
      });
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.fetchHomepageProducts();
    }
  }, {
    key: "renderProducts",
    value: function renderProducts() {
      var _renderRows = [];
      var defaultObj = {};
      defaultObj[this.state.groupName] = {};
      var products = Object.keys(this.state.products).length > 0 ? this.state.products : defaultObj;
      var theproducts = Object(factories["b" /* layoutGrid */])(products[this.state.groupName]);

      if (!this.state.fetching) {
        for (var rows = 0; rows < theproducts.length; rows++) {
          var items = theproducts[rows].map(function (item, index) {
            return external__react__default.a.createElement(product["a" /* default */], {
              key: index,
              product: item,
              columnClass: "col col-xs-12 col-sm-12 col-md-4 col-lg-4"
            });
          });

          _renderRows.push(external__react__default.a.createElement("div", {
            className: "row",
            key: rows
          }, items));
        }
      } else {
        _renderRows.push(external__react__default.a.createElement("div", {
          key: 0,
          className: "row"
        }, external__react__default.a.createElement("div", {
          className: "alert alert-light"
        }, "Loading...")));
      }

      return _renderRows;
    }
  }, {
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        className: "row listings"
      }, external__react__default.a.createElement("div", {
        className: "col-xs-12 col-sm-12 col-md-3 col-lg-3"
      }, external__react__default.a.createElement(filter_Filter, {
        category: decodeURI(this.state.groupName)
      })), external__react__default.a.createElement("div", {
        className: "col-xs-12 col-sm-12 col-md-9 col-lg-9"
      }, external__react__default.a.createElement("div", {
        className: "product-listing"
      }, this.renderProducts(), external__react__default.a.createElement(loadmore_LoadMore, null))));
    }
  }]);

  return Lists;
}(external__react_["Component"]);


// CONCATENATED MODULE: ./pages/more.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return more_More; });
function more__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { more__typeof = function _typeof(obj) { return typeof obj; }; } else { more__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return more__typeof(obj); }

function more__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function more__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function more__createClass(Constructor, protoProps, staticProps) { if (protoProps) more__defineProperties(Constructor.prototype, protoProps); if (staticProps) more__defineProperties(Constructor, staticProps); return Constructor; }

function more__possibleConstructorReturn(self, call) { if (call && (more__typeof(call) === "object" || typeof call === "function")) { return call; } return more__assertThisInitialized(self); }

function more__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function more__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var more_More =
/*#__PURE__*/
function (_Component) {
  more__inherits(More, _Component);

  function More() {
    more__classCallCheck(this, More);

    return more__possibleConstructorReturn(this, (More.__proto__ || Object.getPrototypeOf(More)).apply(this, arguments));
  }

  more__createClass(More, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement(layout["a" /* default */], null, external__react__default.a.createElement(head__default.a, null, external__react__default.a.createElement("link", {
        href: "/css/listings.css",
        rel: "stylesheet"
      })), external__react__default.a.createElement("div", null, external__react__default.a.createElement(list_Lists, {
        groupName: this.props.name
      })));
    }
  }], [{
    key: "getInitialProps",
    value: function getInitialProps(_ref) {
      var name = _ref.query.name;
      return {
        name: name
      };
    }
  }]);

  return More;
}(external__react_["Component"]);



/***/ })
/******/ ]);