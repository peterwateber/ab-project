const gulp = require('gulp')
const nodemon = require('gulp-nodemon')
const sass = require('gulp-sass')
const gconcat = require('gulp-concat')

const DIR = {
    DESTINATION: 'public/',
    SCSS: 'public/css/*.scss',
    JS: 'public/css/*.js'
}

gulp.task('nodemon', () => {
    nodemon({
        script: 'server.js',
        watch: ['server.js', 'gulpfile.js', 'package.json'],
        ext: 'js',
        env: {
            'NODE_ENV': 'development'
        }
    }).on('restart', () => {
        console.log('Server Restarted')
    })
})

gulp.task('core-css', function() {
    return gulp
        .src(['node_modules/bootstrap/scss/bootstrap.scss'])
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(gconcat('core.css'))
        .pipe(gulp.dest(DIR.DESTINATION.concat('css')))
})

gulp.task('core-js', function() {
    return gulp
        .src(['node_modules/bootstrap.native/dist/bootstrap-native-v4.min.js'])
        .pipe(gconcat('core.js'))
        .pipe(gulp.dest(DIR.DESTINATION.concat('js')))
})

gulp.task('page-scss', function() {
    return gulp.src(DIR.SCSS)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(DIR.DESTINATION.concat('css')))
})

gulp.task('watch', () => {
    gulp.watch(DIR.SCSS, ['page-scss'])
})

gulp.task('dev', [
    'nodemon', 
    'core-css',
    'core-js',
    'page-scss',
    'watch'
])