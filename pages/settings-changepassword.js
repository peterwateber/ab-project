import React, { Component } from 'react';
import Head from 'next/head';

import Layout from '../components/layout';
import SettingsPage from '../components/accounts/settings';
import ChangePassword from '../components/accounts/settings.changepassword';

export default class Settings extends Component {
    render() {
        return (
            <Layout>
                <Head>
                    <link rel="stylesheet" href="/css/settings.css" />
                    <link rel="stylesheet" href="/css/accounts.css" />
                </Head>
                <SettingsPage>
                    <ChangePassword />
                </SettingsPage>
            </Layout>
        );
    }
}
