import React, { Component } from 'react'
import Head from 'next/head'

import Layout from '../components/layout'
import LoginPage from '../components/accounts/login'

export default class Login extends Component {
    render() {
        return (
            <Layout>
                <Head>
                    <link href="/css/accounts.css" rel="stylesheet" />
                </Head>
                <div id="account">
                    <LoginPage />
                </div>
            </Layout>
        )
    }
}
