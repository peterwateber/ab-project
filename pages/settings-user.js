import React, { Component } from "react";
import Head from "next/head";

import Layout from "../components/layout";
import SettingsPage from "../components/accounts/settings";
import Users from "../components/accounts/settings.users";

export default class Settings extends Component {
    static getInitialProps({ query: { users } }) {
        return { users };
    }
    render() {
        return (
            <Layout>
                <Head>
                    <link rel="stylesheet" href="/css/settings.css" />
                    <link rel="stylesheet" href="/css/accounts.css" />
                </Head>
                <SettingsPage>
                    <Users source={this.props.users} />
                </SettingsPage>
            </Layout>
        );
    }
}
