import React, { Component } from 'react'
import Head from 'next/head'

import Layout from '../components/layout'
import List from '../components/product-list/list'

export default class More extends Component {
    static getInitialProps({ query: { name } }) {
        return { name }
    }
    render() {
        return (
            <Layout>
                <Head>
                    <link href="/css/listings.css" rel="stylesheet" />
                </Head>
                <div>
                    <List groupName={this.props.name} />
                </div>
            </Layout>
        )
    }
}
