import React, { Component } from 'react';
import Head from 'next/head';

import Layout from '../components/layout';
import SettingsPage from '../components/accounts/settings';
import Profile from '../components/accounts/settings.profile';

export default class Settings extends Component {
    render() {
        return (
            <Layout>
                <Head>
                    <link rel="stylesheet" href="/css/settings.css" />
                    <link rel="stylesheet" href="/css/accounts.css" />
                </Head>
                <SettingsPage>
                    <Profile />
                </SettingsPage>
            </Layout>
        );
    }
}
