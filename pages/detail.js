import React, { Component } from 'react'
import Head from 'next/head'
import axios from 'axios'

import URL from '../constant/urls'
import Layout from '../components/layout'
import ProductDetail from '../components/product-detail/detail'

export default class Detail extends Component {
    static getInitialProps({ query: { name, id } }) {
        return { name, id }
    }
    constructor(props) {
        super(props)
        this.fetchDetails = this.fetchDetails.bind(this)
        this.state = {
            product: {},
            fetching: true
        }
    }
    fetchDetails() {
        axios
            .get(
                URL.Details.concat('?id=eq.')
                    .concat(this.props.id)
                    .concat('&name=eq.')
                    .concat(this.props.name)
            )
            .then(response =>
                this.setState({
                    fetching: false,
                    product: response.data.result.success
                })
            )
            .catch(ex => {})
    }
    componentDidMount() {
        this.fetchDetails()
    }
    render() {
        return (
            <Layout>
                <Head>
                    <link href="/css/detail.css" rel="stylesheet" />
                </Head>
                <div>
                    {!this.state.fetching &&
                        this.state.product.length == 0 && (
                            <div className="row">
                                <div className="col-12">
                                    <div className="alert alert-warning nothing">
                                        No results found
                                    </div>
                                </div>
                            </div>
                        )}
                    {this.state.fetching && (
                        <div className="row">
                            <div className="col-12">
                                <div className="alert alert-light nothing">
                                    Loading...
                                </div>
                            </div>
                        </div>
                    )}
                    {Object.keys(this.state.product).length > 0 && (
                        <ProductDetail
                            product={this.state.product}
                            name={this.props.name}
                            id={this.props.id}
                        />
                    )}
                </div>
            </Layout>
        )
    }
}
