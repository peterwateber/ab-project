import React, { Component } from 'react'
import Link from 'next/link'
import axios from 'axios'

import Layout from '../components/layout'
import Product from '../components/product/product'
import URL from '../constant/urls'
import { layoutGrid } from '../libs/factories'
import Messages from '../constant/messages'

export default class Index extends Component {
    constructor() {
        super()
        this.rows = this.rows.bind(this)
        this.fetchHomepageProducts = this.fetchHomepageProducts.bind(this)
        this.products = this.products.bind(this)
        this.state = {
            groupNames: [],
            products: {},
            message: 'Loading...',
            fetching: true,
            notificationClass: 'light'
        }
    }
    fetchHomepageProducts() {
        axios
            .get(URL.Homepage.home)
            .then(res => res.data)
            .then(response =>
                this.setState({
                    groupNames: Object.keys(response.result.success),
                    products: response.result.success,
                    fetching: false,
                    notificationClass: ''
                })
            )
            .catch(ex => {
                this.setState({
                    message: Messages.Global.FetchingError,
                    notificationClass: 'danger'
                })
            })
    }
    componentDidMount() {
        this.fetchHomepageProducts()
    }
    rows(groupName) {
        let _rows = []
        const products = this.state.products
        const theproducts = layoutGrid(products[groupName])

        for (let rows = 0; rows < theproducts.length; rows++) {
            const items = theproducts[rows].map((item, index) => {
                return (
                    <Product
                        key={index}
                        product={item}
                        columnClass="col-xs-12 col-sm-12 col-md-4 col-lg-3 px-2"
                    />
                )
            })
            _rows.push(
                <div className="row" key={rows}>
                    {items}
                </div>
            )
        }

        return _rows
    }
    products() {
        let _render = []
        if (!this.state.fetching) {
            this.state.groupNames.map((groupName, index) => {
                const _url = '/m/'.concat(groupName.replace(/\s/g, '+'))
                _render.push(
                    <div key={index} className="list-area">
                        <h1 className="group-header">{groupName}</h1>
                        <Link
                            href={{
                                pathname: 'more',
                                query: { name: groupName }
                            }}
                            as={_url}
                        >
                            <a>view more</a>
                        </Link>
                        {this.rows(groupName)}
                    </div>
                )
            })
        } else {
            _render.push(
                <div
                    key={0}
                    className={'alert alert-'.concat(
                        this.state.notificationClass
                    )}
                >
                    {this.state.message}
                </div>
            )
        }
        return _render
    }
    render() {
        return (
            <Layout>
                <div>{this.products()}</div>
            </Layout>
        )
    }
}
