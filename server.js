const express = require('express');
const next = require('next');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const Account = require('./libs/account');
const Config = require('./constant/config');
const httpCodes = require('./constant/httpCodes');
const Messages = require('./constant/messages');
const session = require('express-session');
const dev = process.env.NODE_ENV !== 'production';
const nextApp = next({
    dev
});
const handle = nextApp.getRequestHandler();

nextApp
    .prepare()
    .then(() => {
        const app = express();

        app.use(cookieParser());
        app.use(bodyParser.json());
        app.use(
            session({
                secret: '$3Kr3+ k3y',
                resave: false,
                saveUninitialized: true
            })
        );
        app.set('views', 'pages');
        app.use(express.static('public'));

        const userId = req => {
            return (
                req.cookies[Config.COOKIE.loginCookie] ||
                req.session[Config.COOKIE.loginSession]
            );
        };

        /** DB TESTING */
        app.post('/api/:filename', (req, res) => {
            let file = require('./samplejson/' + req.params.filename);
            res.send(file);
        });
        app.get('/api/:filename', (req, res) => {
            let file = require('./samplejson/' + req.params.filename);
            res.send(file);
        });
        /** DB TESTING */

        app.post('/ajax', (req, res) => {
            switch (req.body.action) {
                case 'header':
                    Account.header(userId(req), {
                        success(data) {
                            res.send({
                                success: data.operationCode === httpCodes.SUCCESS,
                                result: data.success
                            });
                        },
                        error(data) {
                            res.send(data);
                        }
                    });
                    break;
                case 'login':
                    Account.login(req.body, {
                        success(data) {
                            let obj = {
                                success: true
                            };
                            if (data.operationCode === httpCodes.SUCCESS) {
                                if (req.body.rem) {
                                    res.cookie(
                                        Config.COOKIE.loginCookie,
                                        data.success.id,
                                        Config.COOKIE.config
                                    );
                                } else {
                                    req.session[Config.COOKIE.loginSession] =
                                        data.success.id;
                                }
                                obj = Object.assign(obj, data, {
                                    redirect: '/'
                                });
                            } else {
                                obj = {
                                    success: false,
                                    message: Messages.Form.Login[data.operationCode]
                                };
                            }
                            res.send(obj);
                        },
                        error(err) {
                            console.log(err.message);
                            res.send({
                                success: false,
                                message: Messages.Global.ServerError
                            });
                        }
                    });
                    break;
            }
        });

        /**
         * LISTINGS PAGE
         * **/

        app.get('/more', (req, res) => {
            res.redirect('/error');
        });
        app.get('/m/:name', (req, res) => {
            const actualPage = '/more';
            const name = { name: req.params.name };
            nextApp.render(req, res, actualPage, name);
        });

        /**
         * DETAILS PAGE
         */
        app.get('/detail', (req, res) => {
            res.redirect('/error');
        });
        app.get('/d/:name/:id', (req, res) => {
            const actualPage = '/detail';
            const query = {
                name: req.params.name,
                id: req.params.id
            };
            nextApp.render(req, res, actualPage, query);
        });

        /**
         * SETTINGS PAGE
         */

        app.get('/settings', (req, res) => {
            res.redirect('/settings/user');
        });

        app.get('/settings/:page', (req, res) => {
            const actualPage = '/settings-' + req.params.page;
            let page = { page: req.params.page };

            switch (req.params.page) {
                case 'user':
                    page.users = [{
                        id: '1',
                        name: 'peter',
                        email: 'peter@peter'
                    }];
                    break;
                case 'profile':
                    break;
                case 'changepassword':
                    break;
            }
            nextApp.render(req, res, actualPage, page);
        });

        /**
         * ALWAYS PUT HANDLER BELOW
         * **/

        app.get('*', (req, res) => {
            return handle(req, res);
        });

        /****/

        app.listen(Config.port, err => {
            if (err) throw err;
            console.log(`Server @ http://localhost:${Config.port}`);
        });
    })
    .catch(ex => {
        console.error(ex.stack);
        process.exit(1);
    });