module.exports = {
    SUCCESS: 200,
    EmailAlreadyExists: 'Reg_EmailExists',
    PasswordToShort: 'PasswdToShort',
    InvalidUsernamePassword: 'InvalidUserPass'
}