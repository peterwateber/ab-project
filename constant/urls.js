const port = require('./config').port
const BASE_URL = 'http://localhost:' + port

module.exports = {
    Headers: {},
    Accounts: {
        header: BASE_URL + '/api/user',
        displayName: BASE_URL + '/api/user',
        register:  BASE_URL + '/api/register',
        login: BASE_URL + '/api/login'

    },
    Homepage: {
        home: BASE_URL + '/api/products'
    },
    Details: BASE_URL + '/api/product'
}
