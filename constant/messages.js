module.exports = {
    Global: {
        FetchingError: 'Error fetching...',
        ServerError: 'We encountered an error. Please try again later.'
    },
    Form: {
        Login: {
            InvalidUsernamePassword: 'Invalid username or password.'
        },
        Registration: {
            InvalidEmail: 'Email is not valid.',
            PasswordMatch: 'Confirm Password must match with password.',
            BirthdayFormat: '',
            Required: {
                first_name: 'First Name is required.',
                last_name: 'Last Name is required.',
                email: 'Email Address is required.',
                birthday: 'Birthday is required.',
                password: 'Password is required.',
                confirm_password: 'Confirm Password is required.'
            },
            Success:
                'Email verification has been sent. Please check your email inbox for activation.',
            RegisterEmail: function(name, url) {
                return `<p style="margin-top: 0">
                            Hello ${name},
                        </p>
                        <p>
                            Please active your account below to proceed.
                        </p>
                        <p>
                            <a href="${url}" style="background: #218838; text-decoration: none; color: #fff; padding: 15px; font-size: 17px; text-decoration: none;">Activate</a>
                        </p>`
            }
        }
    }
}
