module.exports = {
    port: 3001,
    TIMEOUT: 30000,
    COOKIE: {
        loginCookie: 'project_cookie',
        loginSession: 'project_session',
        config: {
            maxAge: 60 * 60 * 24 * 365,
            httpOnly: true, // http only, prevents JavaScript cookie access
            secure: false // cookie must be sent over https / ssl
        }
    }
}
