String.prototype.isValidEmail = function() {
    const regexEmail = /^(([^<>()\[\]\.,:\s@\"]+(\.[^<>()\[\]\.,:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,:\s@\"]+\.)+[^<>()[\]\.,:\s@\"]{2,})$/i
    return this && regexEmail.test(this)
}

String.prototype.isValidRegistrationBirthday = function() {
    const regEx = /^\d{4}-\d{2}-\d{2}$/
    if (!this.match(regEx)) return false // Invalid format
    const d = new Date(this)
    if (!d.getTime() && d.getTime() !== 0) return false // Invalid date
    return d.toISOString().slice(0, 10) === dateString
}

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1)
}