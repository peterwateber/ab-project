import React, { Component } from 'react'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'

export class Form extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: props.name,
            disabled: props.disabled || '',
            placeholder: props.placeholder || '',
            id: props.id || '',
            className: props.className || '',
            type: props.type || 'text',
            text: props.text || '',
            message: props.message || ''
        }
    }
    render() {
        const formClass = this.state.type === 'checkbox' ? '' : 'form-field '
        const placeholderClass = this.props.value.length > 0 ? 'extended' : ''
        let inputfield = (<input type={this.state.type}
            id={this.state.id}
            className={this.state.type !== 'checkbox' ? 'form-control '.concat(this.state.className) : this.state.className}
            name={this.state.name}
            onChange={this.props.onChange}
            disabled={this.state.disabled}
            value={this.props.value}
        />)
        return <div className={formClass.concat(this.state.message ? 'field-error' : '')}>
            <div className="form-group">
                <label htmlFor={this.state.id} className={"placeholder ".concat(placeholderClass)}>{this.state.placeholder}</label>
                {inputfield} {this.state.text || ''}
                {
                    this.state.type !== 'checkbox' && <div className="placeholder-label">
                        <FontAwesomeIcon icon="exclamation-circle" />
                    </div>
                }
            </div>
            <div className="feedback">{this.state.message}</div>
        </div>
    }
}
