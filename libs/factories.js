import { Form } from './form.js'

export const FormInput = Form

export const layoutGrid = products => {
    let columns = []
    try {
        let cols,
            totalColumn = products.length,
            chunk = 4
        for (cols = 0; cols < totalColumn; cols += chunk) {
            columns.push(products.slice(cols, cols + chunk))
        }
    } catch (ex) {}
    return columns
}

export const pagination = (curr, lst) => {
    let current = curr,
        last = lst,
        delta = 2,
        left = current - delta,
        right = current + delta + 1,
        range = [],
        rangeWithDots = [],
        l;

    for (let i = 1; i <= last; i++) {
        if (i == 1 || i == last || (i >= left && i < right)) {
            range.push(i);
        }
    }

    for (let i of range) {
        if (l) {
            if (i - l === 2) {
                rangeWithDots.push(l + 1);
            } else if (i - l !== 1) {
                rangeWithDots.push("...");
            }
        }
        rangeWithDots.push(i);
        l = i;
    }

    return rangeWithDots;
};
