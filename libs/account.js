const axios = require('axios')
const Messages = require('../constant/messages')
const URL = require('../constant/urls')
require('./prototype')

class AccountLib {
    header(userid, callback) {
        if (userid) {
            axios
                .post(URL.Accounts.displayName, {
                    _id: userid
                })
                .then(result => result.data[0].result)
                .then(data => {
                    callback && callback.success(data)
                })
                .catch(err => {
                    callback && callback.error(err)
                })
        } else {
            callback &&
                callback.success({
                    success: false
                })
        }
    }
    validateRegistration(user) {
        let error = {},
            className,
            success = false

        for (var prop in user) {
            if (!user[prop]) {
                error[prop] = Messages.Form.Registration.Required[prop]
            }

            if (prop && user[prop]) {
                switch (prop) {
                    case 'email':
                        if (!user.email.isValidEmail()) {
                            error.email =
                                Messages.Form.Registration.InvalidEmail
                        }
                        break
                    case 'confirm_password':
                        if (
                            user.confirm_password !== user.password &&
                            user.password
                        ) {
                            error.confirm_password =
                                Messages.Form.Registration.PasswordMatch
                        }
                        break
                }
            }
        }
        if (Object.keys(error).length === 0) {
            className = 'success'
            success = true
        } else {
            className = 'error'
        }
        return {
            success: success,
            error: error,
            className: className
        }
    }
    register(user, callback) {
        axios
            .post(URL.Accounts.register, user)
            .then(result => result.data[0].result)
            .then(data => {
                callback && callback.success(data)
            })
            .catch(ex => {
                callback && callback.error(ex)
            })
    }
    login(user, callback) {
        axios
            .post(URL.Accounts.login, {
                _uname: user.email,
                _pass: user.password
            })
            .then(res => {
                callback && callback.success(res.data.result)
            })
            .catch(err => {
                callback && callback.error(err)
            })
    }
}

module.exports = new AccountLib()
