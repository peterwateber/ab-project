import React from 'react';

import PageTooltip from '../misc/tooltip';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

export const Tbody = props => {
    return (
        <tbody>
            {props.source.length === 0 && (
                <tr>
                    <td colSpan="4">Empty Table</td>
                </tr>
            )}
            {props.source.map((row, index) => {
                return (
                    <tr key={index}>
                        {props.keys.map((k, index) => {
                            return <td key={index}>{row[k]}</td>;
                        })}
                        <td className="table-actions">
                            <a href="#" className="px-1" id={'edit-' + index}>
                                <FontAwesomeIcon icon="pen-square" />
                                <PageTooltip
                                    position="bottom"
                                    target={'edit-' + index}
                                    message="Edit"
                                />
                            </a>
                            <a href="#" className="px-1" id={'delete-' + index}>
                                <FontAwesomeIcon icon="trash-alt" />
                                <PageTooltip
                                    position="bottom"
                                    target={'delete-' + index}
                                    message="Delete"
                                />
                            </a>
                        </td>
                    </tr>
                );
            })}
        </tbody>
    );
};
