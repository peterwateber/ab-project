import React, { Component } from 'react';
import { Table } from 'reactstrap';
import { Tbody } from './table.body';
import { Tpages } from './table.pages';
import { FormInput } from '../../libs/factories';

export class DataTable extends Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.onClickPage = this.onClickPage.bind(this);
        this.state = {
            search_key: '',
            source: props.source,
            originalSource: props.source,
            currentPage: 1,
            count: props.count
        };
    }
    onChange(e) {
        const searchKey = e.target.value;
        let newSource = this.state.source.filter(item => {
            return this.props.keys.some(function(key) {
                return item[key]
                    .toString()
                    .toLocaleLowerCase()
                    .includes(searchKey.toLocaleLowerCase());
            });
        });
        if (!searchKey) {
            newSource = this.state.originalSource;
        }
        this.setState({
            search_key: searchKey,
            source: newSource,
            count: newSource.length
        });
    }
    onClickPage(e, page) {
        e.preventDefault();
        this.setState({
            currentPage: page,
            source: this.state.originalSource
        });
    }
    render() {
        const offset = this.state.currentPage * this.props.limit;
        const start = offset - this.props.limit;
        const source = this.state.source.slice(start, offset);
        return (
            <div>
                <div>
                    <form autoComplete="off">
                        <FormInput
                            name="search_key"
                            id="search"
                            placeholder="Search Table"
                            onChange={this.onChange}
                            disabled={this.state.loading}
                            message=''
                            value={this.state.search_key}
                        />
                    </form>
                </div>
                <Table responsive borderless size="sm">
                    <thead>{this.props.children}</thead>
                    <Tbody source={source} keys={this.props.keys} />
                </Table>
                <Tpages
                    onClickPage={this.onClickPage}
                    currentPage={this.state.currentPage}
                    count={this.state.count}
                    limit={this.props.limit}
                />
            </div>
        );
    }
}
