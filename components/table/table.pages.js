import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import { pagination } from '../../libs/factories';

import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

export const Tpages = props => {
    let arr = [];
    const lastPage = Math.ceil(props.count / props.limit);
    const pages = pagination(props.currentPage, lastPage);
    for (let i = 0; i < pages.length; i++) {
        arr.push(
            <PaginationItem key={i}>
                <PaginationLink
                    key={i}
                    href="#"
                    onClick={e => props.onClickPage(e, i + 1)}
                >
                    {pages[i]}
                </PaginationLink>
            </PaginationItem>
        );
    }
    return (
        <div>
            {props.count > 0 && (
                <Pagination>
                    <PaginationItem disabled={props.currentPage === 1}>
                        <PaginationLink
                            href="#"
                            onClick={e =>
                                props.onClickPage(e, props.currentPage - 1)
                            }
                        >
                            Prev
                        </PaginationLink>
                    </PaginationItem>
                    {arr}
                    <PaginationItem disabled={props.currentPage === lastPage}>
                        <PaginationLink
                            href="#"
                            onClick={e =>
                                props.onClickPage(e, props.currentPage + 1)
                            }
                        >
                            Next
                        </PaginationLink>
                    </PaginationItem>
                </Pagination>
            )}
        </div>
    );
};
