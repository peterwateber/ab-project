import React, { Component } from 'react'
import PageTooltip from './tooltip'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'

export default class Verified extends Component {
    constructor(props) {
        super(props)
        this.state = {
            message: props.message || 'Verified',
            tooltipPosition: props.position || 'top'
        }
    }
    render() {
        return (
            <div className="verified" id="verified">
                <FontAwesomeIcon icon="check-circle" />
                <PageTooltip
                    position={this.state.tooltipPosition}
                    target="verified"
                    message={this.state.message}
                />
            </div>
        )
    }
}
