import React, { Component } from 'react'
import { Tooltip } from 'reactstrap'

export default class PageTooltip extends Component {
    constructor(props) {
        super(props)
        this.toggle = this.toggle.bind(this)
        this.state = {
            tooltipOpen: false
        }
    }

    toggle() {
        this.setState({
            tooltipOpen: !this.state.tooltipOpen
        })
    }

    render() {
        const placement = this.props.position ? this.props.position : 'top'
        return (
            <Tooltip
                placement={placement}
                isOpen={this.state.tooltipOpen}
                target={this.props.target}
                toggle={this.toggle}
            >
                {this.props.message}
            </Tooltip>
        )
    }
}
