import React, { Component } from "react";
import Link from "next/link";
import ImageLoader from "react-load-image";

import FontAwesomeIcon from "@fortawesome/react-fontawesome";

export default class Product extends React.Component {
    render() {
        const detailsURL =
            "/d/" +
            this.props.product.name.replace(/\s/g, "-") +
            "/" +
            this.props.product.id;
        return (
            <div className={this.props.columnClass}>
                <div className="card box-shadow">
                    <ImageLoader src={this.props.product.images.preview}>
                        <img className="card-img-top" />
                        <div>Error!</div>
                        <img src="/img/ajax.gif" />
                    </ImageLoader>
                    <div className="card-body">
                        <span className="badge badge-success">
                            ${this.props.product.price}
                        </span>
                        <p className="card-text">{this.props.product.name}</p>
                        <a
                            href="#"
                            className="btn btn-sm btn-outline-secondary btn-product">
                            <span>
                                <FontAwesomeIcon icon="cart-plus" /> Add to cart
                            </span>
                        </a>
                        <Link
                            href={{
                                pathname: "detail",
                                query: {
                                    name: this.props.product.name.replace(
                                        /\s/g,
                                        "-"
                                    ),
                                    id: this.props.product.id
                                }
                            }}
                            as={detailsURL}>
                            <a className="btn btn-sm btn-success btn-product">
                                View Details
                            </a>
                        </Link>
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}
