import React, { Component } from 'react'

export default class Filter extends Component {
    render() {
        return (
            <ul className="list-group">
                <li className="list-group-item list-group-header">
                    {this.props.category}
                </li>
            </ul>
        )
    }
}
