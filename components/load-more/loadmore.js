import React, { Component } from 'react'

import FontAwesomeIcon from '@fortawesome/react-fontawesome'

export default class LoadMore extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 load-more text-center">
                    <FontAwesomeIcon icon="ellipsis-h" />
                </div>
            </div>
        )
    }
}
