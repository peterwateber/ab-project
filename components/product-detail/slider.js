import React, { Component } from 'react'
import { Carousel, CarouselItem } from 'reactstrap'

import ImageLoader from 'react-load-image'
import SliderIndicator from './slider-indicator'
import SliderControl from './slider-control'

import FontAwesomeIcon from '@fortawesome/react-fontawesome'

export default class ItemSlider extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activeIndex: 0,
            images: props.images
        }
        this.next = this.next.bind(this)
        this.previous = this.previous.bind(this)
        this.setSliderIndicator = this.setSliderIndicator.bind(this)
        this.onExiting = this.onExiting.bind(this)
        this.onExited = this.onExited.bind(this)
    }

    onExiting() {
        this.animating = true
    }

    onExited() {
        this.animating = false
    }

    next() {
        if (this.animating) return
        const nextIndex =
            this.state.activeIndex === this.props.images.original.length - 1
                ? 0
                : this.state.activeIndex + 1
        this.setState({ activeIndex: nextIndex })
    }

    previous() {
        if (this.animating) return
        const nextIndex =
            this.state.activeIndex === 0
                ? this.props.images.original.length - 1
                : this.state.activeIndex - 1
        this.setState({ activeIndex: nextIndex })
    }

    setSliderIndicator(newIndex) {
        if (this.animating) return
        this.setState({ activeIndex: newIndex })
    }

    componentWillReceiveProps(props) {
        this.setState({
            images: props.images
        })
    }

    render() {
        const { activeIndex } = this.state
        const images =
            Object.keys(this.state.images).length > 0 ? this.state.images : {}
        let slides = []
        let thumbnails = []
        if (Object.keys(images).length > 0) {
            slides = images.original.map((item, index) => {
                return (
                    <CarouselItem
                        className="text-center"
                        onExiting={this.onExiting}
                        onExited={this.onExited}
                        key={index}
                    >
                        <ImageLoader src={item}>
                            <img />
                            <div>Error!</div>
                            <img src="/img/ajax.gif" />
                        </ImageLoader>
                    </CarouselItem>
                )
            })
            thumbnails = (
                <SliderIndicator
                    onClickHandler={this.setSliderIndicator}
                    activeIndex={activeIndex}
                    thumbnails={images.thumbnails}
                    targetId="carousel"
                />
            )
        }
        return (
            <Carousel
                id="carousel"
                activeIndex={activeIndex}
                next={this.next}
                previous={this.previous}
            >
                {slides}
                {thumbnails}
                <SliderControl
                    onPrevClickHandler={this.previous}
                    onNextClickHandler={this.next}
                />
            </Carousel>
        )
    }
}
