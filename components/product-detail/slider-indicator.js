import React from 'react'
import ImageLoader from 'react-load-image'

export default class SliderIndicator extends React.Component {
    constructor(props) {
        super(props)
        this.renderSliderIndicator = this.renderSliderIndicator.bind(this)
        this.onClickHandler = this.onClickHandler.bind(this)
        this.state = {
            activeIndex: props.activeIndex
        }
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            activeIndex: nextProps.activeIndex
        })
    }
    onClickHandler(newIndex) {
        this.setState({
            activeIndex: newIndex
        })
        this.props.onClickHandler(newIndex)
    }
    renderSliderIndicator() {
        let _render = []
        const thumbnails = this.props.thumbnails
        for (let i = 0; i < thumbnails.length; i++) {
            const active = i === this.state.activeIndex ? 'active' : ''
            _render.push(
                <li
                    key={i}
                    onClick={() => this.onClickHandler(i)}
                    data-target={this.props.targetId}
                    data-slide-to={i}
                    className={active}
                >
                    <ImageLoader src={thumbnails[i]}>
                        <img />
                        <div>Error!</div>
                        <img src="/img/ajax.gif" />
                    </ImageLoader>
                </li>
            )
        }
        return _render
    }
    render() {
        return (
            <ol className="carousel-indicators">
                {this.renderSliderIndicator()}
            </ol>
        )
    }
}
