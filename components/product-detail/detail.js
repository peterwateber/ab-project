import React, { Component } from 'react'
import Slider from './slider'

import FontAwesomeIcon from '@fortawesome/react-fontawesome'

export default class Detail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            product: props.product
        }
    }
    render() {
        const resultsCount = Object.keys(this.state.product).length
        return (
            <div className="page">
                <div className="details">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <Slider images={this.state.product.images} />
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div className="card">
                                <div className="card-body">
                                    <h2 className="card-title">
                                        {this.state.product.name}
                                    </h2>
                                    <h5 className="card-title">
                                        Static Label 2
                                    </h5>
                                    <p className="card-text">
                                        {this.state.product.description}
                                    </p>
                                </div>
                                <div className="card-footer product-actions">
                                    <div className="float-left">
                                        <strong>
                                            Price: ${this.state.product.price}
                                        </strong>
                                    </div>
                                    <div className="float-right">
                                        <a href="#">
                                            <FontAwesomeIcon icon="heart" />
                                        </a>
                                        <a href="#">
                                            <FontAwesomeIcon icon="cart-plus" />
                                        </a>
                                        <a href="#" className="btn btn-primary">
                                            Go somewhere
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="lists">
                    <div className="container mc" />
                </div>
            </div>
        )
        /*return <div className="page">
            <div className="details">
                <div className="container mc">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <Slider products={this.props.products}></Slider>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div className="card">
                                <div className="card-body">
                                    <h2 className="card-title">{this.state.product.name}</h2>
                                    <h5 className="card-title">Label 2</h5>
                                    <p className="card-text">
                                        {this.state.product.description}
                                    </p>
                                </div>
                                <div className="card-footer">
                                    <div className="float-left">
                                        <ul className="list-inline">
                                            <li className="list-inline-item">Sizes</li>
                                            <li className="list-inline-item">
                                                <a href="#">S</a>
                                            </li>
                                            <li className="list-inline-item">
                                                <a href="#">M</a>
                                            </li>
                                            <li className="list-inline-item">
                                                <a href="#">L</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="float-right">
                                        <ul className="list-inline">
                                            <li className="list-inline-item">Quantity</li>
                                            <li className="list-inline-item">
                                                <a href="#">+</a>
                                            </li>
                                            <li className="list-inline-item">3</li>
                                            <li className="list-inline-item">
                                                <a href="#">-</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="card-footer product-actions">
                                    <div className="float-left">
                                        <strong>Price: $120</strong>
                                    </div>
                                    <div className="float-right">
                                        <a href="#">
                                            <FontAwesomeIcon icon={faHeart}/>
                                        </a>
                                        <a href="#">
                                            <FontAwesomeIcon icon="cart-plus"/>
                                        </a>
                                        <a href="#" className="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="lists">
                <div className="container mc">
                </div>
            </div>
        </div>*/
    }
}
