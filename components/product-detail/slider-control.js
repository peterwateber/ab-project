import React from 'react'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'

export default class SliderControl extends React.Component {
    render() {
        return (
            <div>
                <a
                    className="carousel-control-prev"
                    href="javascript:void(0)"
                    role="button"
                    onClick={this.props.onPrevClickHandler}
                >
                    <FontAwesomeIcon icon="angle-left" />
                </a>
                <a
                    className="carousel-control-next"
                    href="javascript:void(0)"
                    role="button"
                    onClick={this.props.onNextClickHandler}
                >
                    <FontAwesomeIcon icon="angle-right" />
                </a>
            </div>
        )
    }
}
