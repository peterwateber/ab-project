import React, { Component } from "react";
import Head from "next/head";
import Header from "./header";

import fontawesome from "@fortawesome/fontawesome";
import {
    faSearch,
    faCartPlus,
    faAngleDoubleRight,
    faEllipsisH,
    faHeart,
    faPenSquare,
    faCheckCircle,
    faExclamationCircle,
    faTrashAlt
} from "@fortawesome/fontawesome-free-solid";

fontawesome.library.add(
    faSearch,
    faCartPlus,
    faAngleDoubleRight,
    faEllipsisH,
    faHeart,
    faPenSquare,
    faCheckCircle,
    faExclamationCircle,
    faTrashAlt
);

export default class Index extends Component {
    render() {
        return (
            <div>
                <Head>
                    <link href="/css/core.css" rel="stylesheet" />
                    <link href="/css/style.css" rel="stylesheet" />
                </Head>
                <Header />
                <div className="container mc">{this.props.children}</div>
                <script src="/js/core.js" />
            </div>
        );
    }
}
