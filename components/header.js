import React from 'react'
import {
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap'
import axios from 'axios'

import Config from '../constant/config'

import FontAwesomeIcon from '@fortawesome/react-fontawesome'

export default class Header extends React.Component {
    constructor() {
        super()
        this.toggle = this.toggle.bind(this)
        this.state = {
            name: 'Login',
            islogin: false,
            user_url: '/login',
            dropdownOpen: false
        }
    }
    componentDidMount() {
        const headerXhr = axios.create()

        headerXhr.defaults.timeout = Config.TIMEOUT

        axios
            .post('/ajax', {
                action: 'header'
            })
            .then(result => result.data)
            .then(data => {
                if (data.success) {
                    this.setState({
                        name: data.result.firstname,
                        islogin: data.success,
                        user_url: '#'
                    })
                }
            })
            .catch(err => {})
    }
    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        })
    }
    render() {
        var hideNavLinks = false

        if (this.props.hideNavLinks === 'true') {
            hideNavLinks = true
        }

        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container">
                    <a href="/" className="navbar-brand">
                        Project
                    </a>
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbar"
                        aria-controls="navbarsExample07"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span className="navbar-toggler-icon" />
                    </button>
                    <div className="collapse navbar-collapse" id="navbar">
                        {hideNavLinks === false && (
                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item">
                                    <a className="nav-link" href="#">
                                        Inactive Link
                                    </a>
                                </li>
                                <li className="nav-item dropdown">
                                    <a
                                        href="#"
                                        className="nav-link dropdown-toggle"
                                        id="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                        data-toggle="dropdown"
                                    >
                                        Dropdown
                                    </a>
                                    <div
                                        className="dropdown-menu"
                                        aria-labelledby="dropdown"
                                    >
                                        <a className="dropdown-item" href="#">
                                            Action
                                        </a>
                                        <a className="dropdown-item" href="#">
                                            Another action
                                        </a>
                                        <a className="dropdown-item" href="#">
                                            Something else here
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        )}
                        <form className="form-inline ml-auto form-navigation">
                            <div className="form-navigation-wrapper">
                                <span className="fa search-area">
                                    <span className="search-icon">
                                        <FontAwesomeIcon icon="search" />
                                    </span>
                                    <span className="form-input-holder">
                                        <input
                                            className="form-control"
                                            type="text"
                                            placeholder="Search"
                                            aria-label="Search"
                                        />
                                    </span>
                                </span>
                                <ButtonDropdown
                                    className="fa"
                                    isOpen={this.state.dropdownOpen}
                                    toggle={this.toggle}
                                >
                                    {!this.state.islogin && (
                                        <DropdownToggle
                                            href={this.state.user_url}
                                        >
                                            {this.state.name}
                                        </DropdownToggle>
                                    )}
                                    {this.state.islogin && (
                                        <DropdownToggle caret>
                                            {this.state.name}
                                        </DropdownToggle>
                                    )}
                                    {this.state.islogin && (
                                        <DropdownMenu>
                                            <DropdownItem href="/settings">
                                                My account
                                            </DropdownItem>
                                            <DropdownItem href="/logout">
                                                Logout
                                            </DropdownItem>
                                        </DropdownMenu>
                                    )}
                                </ButtonDropdown>

                                <a href="/details" className="fa">
                                    <FontAwesomeIcon icon="cart-plus" />
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </nav>
        )
    }
}
