import React, { Component } from 'react'
import axios from 'axios'
import URL from '../../constant/urls'
import { layoutGrid } from '../../libs/factories'

import Product from '../product/product'
import LoadMore from '../load-more/loadmore'
import Filter from '../product-filter/filter'

export default class Lists extends Component {
    constructor(props) {
        super(props)
        this.renderProducts = this.renderProducts.bind(this)
        this.state = {
            groupName: props.groupName.replace(/\+/g, ' ') || '',
            products: {},
            fetching: true
        }
    }
    fetchHomepageProducts() {
        axios
            .get(URL.Homepage.home)
            .then(response => response.data.result)
            .then(response => {
                this.setState({
                    fetching: false,
                    products: response.success
                })
            })
            .catch(ex => {
                console.log(ex)
            })
    }
    componentDidMount() {
        this.fetchHomepageProducts()
    }
    renderProducts() {
        let _renderRows = []
        const defaultObj = {}
        defaultObj[this.state.groupName] = {}
        const products =
            Object.keys(this.state.products).length > 0
                ? this.state.products
                : defaultObj
        const theproducts = layoutGrid(products[this.state.groupName])

        if (!this.state.fetching) {
            for (let rows = 0; rows < theproducts.length; rows++) {
                const items = theproducts[rows].map(function(item, index) {
                    return (
                        <Product
                            key={index}
                            product={item}
                            columnClass="col col-xs-12 col-sm-12 col-md-4 col-lg-4"
                        />
                    )
                })
                _renderRows.push(
                    <div className="row" key={rows}>
                        {items}
                    </div>
                )
            }
        } else {
            _renderRows.push(
                <div key={0} className="row">
                    <div className="alert alert-light">Loading...</div>
                </div>
            )
        }
        return _renderRows
    }
    render() {
        return (
            <div className="row listings">
                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <Filter category={decodeURI(this.state.groupName)} />
                </div>
                <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                    <div className="product-listing">
                        {this.renderProducts()}
                        <LoadMore />
                    </div>
                </div>
            </div>
        )
    }
}
