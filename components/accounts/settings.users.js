import React, { Component } from 'react';
import { Row, Col, Card, CardBody } from 'reactstrap';
import { DataTable } from '../table/table';

export default class SettingsUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keys: ['id', 'name', 'email'],
            source: props.source,
            count: 0
        };
    }
    render() {
        return (
            <DataTable
                style="dark"
                limit="5"
                keys={this.state.keys}
                source={this.state.source}
                count={this.state.count}
            >
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </DataTable>
        );
    }
}
