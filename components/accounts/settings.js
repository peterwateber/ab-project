import React, { Component } from "react";
import Link from "next/link";

import { Row, Col, Card, CardBody, ListGroup, ListGroupItem } from "reactstrap";
import ImageLoader from "react-load-image";

import Verified from "../misc/verified";

export default class Layout extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: "1"
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    render() {
        return (
            <div id="account">
                <div className="page-banner">
                    <div className="container mc">
                        <Col lg="12" md="12" sm="12">
                            <span>Settings</span>
                        </Col>
                    </div>
                </div>
                <Row>
                    <Col lg="4" md="4" sm="12" xs="12">
                        <Card className="user-profile">
                            <div className="user-header">
                                <div className="user-header-wrapper">
                                    <div className="user-avatar">
                                        <ImageLoader src="http://via.placeholder.com/200x200/000/fff/">
                                            <img className="user-profile-pic rounded-circle" />
                                            <div>Error!</div>
                                            <img src="/img/ajax.gif" />
                                        </ImageLoader>
                                    </div>
                                    <div className="user-identity">
                                        <h5>
                                            Peter
                                            <Verified position="right" />
                                        </h5>
                                        <span>Joined Dec 12, 2012</span>
                                    </div>
                                </div>
                            </div>
                            <CardBody>
                                <ListGroup>
                                    <ListGroupItem>
                                        <Link href="/settings/user">
                                            <a>Users</a>
                                        </Link>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Link href="/settings/profile">
                                            <a>My Profile</a>
                                        </Link>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Link href="/settings/changepassword">
                                            <a>Change Password</a>
                                        </Link>
                                    </ListGroupItem>
                                </ListGroup>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col>{this.props.children}</Col>
                </Row>
            </div>
        );
    }
}
