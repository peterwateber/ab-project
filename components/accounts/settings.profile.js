import React, { Component } from "react";
import Head from "next/head";

import "../product/product";
import Product from "../product/product";

export default class SettingsProfile extends Component {
    render() {
        const item = {
            id: 1,
            name: "item 1",
            description: "description for item 2",
            price: 101.7,
            images: {
                original: [
                    "http://via.placeholder.com/280x365/000/fff/",
                    "http://via.placeholder.com/280x365/000/fff/",
                    "http://via.placeholder.com/280x365/000/fff/"
                ],
                preview:
                    "http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg",
                thumbnails: [
                    "http://via.placeholder.com/90x70/000/fff/",
                    "http://via.placeholder.com/90x70/000/fff/",
                    "http://via.placeholder.com/90x70/000/fff/"
                ]
            }
        };
        return (
            <div>
                <Head>
                    <link rel="stylesheet" href="/css/posting.css" />
                </Head>
                <div className="posting">
                    <h4>Product Listing</h4>
                    <hr />
                    <form>
                        <div className="form-group">
                            <label>Product Name</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Name"
                            />
                        </div>
                        <div className="form-group">
                            <label>Product Price</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Price"
                            />
                        </div>
                        <div className="text-right">
                            <input
                                type="submit"
                                className="btn btn-primary"
                                value="Post"
                            />
                        </div>
                    </form>
                </div>
                <div className="row">
                    <Product
                        product={item}
                        columnClass="col-xs-12 col-sm-12 col-md-4 col-lg-4"
                    >
                        <a href="#" className="btn btn-sm btn-danger btn-product">
                            Delete
                        </a>
                    </Product>
                </div>
            </div>
        );
    }
}
