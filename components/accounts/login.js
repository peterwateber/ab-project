import React, { Component }     from 'react'
import ReactDOM from 'react-dom'
import axios from 'axios'

import { FormInput } from '../../libs/factories'

export default class Login extends Component {
    constructor() {
        super()
        this.onChange = this.onChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.state = {
            formData: {
                email: '',
                password: '',
                rem: ''
            },
            loading: false,
            error: {},
            message: '',
            action: 'login'
        }
    }
    handleSubmit(e) {
        e.preventDefault()
        const self = this
        self.setState({
            loading: true
        })
        const formData = Object.assign({
            action: self.state.action
        }, self.state.formData)

        axios
            .post('/ajax', formData)
            .then(result => result.data)
            .then(msg => {
                setTimeout(() => {
                    if (msg.success) {
                        window.location = msg.redirect
                    } else {
                        self.setState({
                            notificationClass: 'danger',
                            loading: false,
                            message: msg.message
                        })
                    }
                }, 200)
            })
            .catch(ex => {
                
            })
    }
    onChange(e) {
        const state = this.state.formData
        state[e.target.name] = e.target.value

        if (e.target.type === 'checkbox') {
            state[e.target.name] = e.target.checked
        }

        this.setState(state)
    }
    render() {
        const loading = this.state.loading ? 'form-loading' : ''
        return (
            <form
                autoComplete="off"
                onSubmit={this.handleSubmit}
                className="form-signin"
            >
                <h1 className="h3 mb-3 font-weight-normal">Please Login</h1>
                {this.state.message && (
                    <div className="alert alert-danger">
                        {this.state.message}
                    </div>
                )}
                <FormInput
                    name="email"
                    id="email"
                    placeholder="Email Address"
                    onChange={this.onChange}
                    disabled={this.state.loading}
                    message={this.state.error.email}
                    value={this.state.formData.email}
                />

                <FormInput
                    name="password"
                    id="password"
                    type="password"
                    placeholder="Password"
                    onChange={this.onChange}
                    disabled={this.state.loading}
                    message={this.state.error.password}
                    value={this.state.formData.password}
                />

                <div className="checkbox mb-3">
                    <label>
                        <FormInput
                            type="checkbox"
                            name="rem"
                            text="Remember me"
                            onChange={this.onChange}
                            value={this.state.formData.rem}
                        />
                    </label>
                </div>
                <button
                    disabled={this.state.loading}
                    className={'btn btn-lg btn-success btn-block btn-login '.concat(
                        loading
                    )}
                    type="submit"
                >
                    Login
                </button>
                <a
                    href="/register"
                    className="btn btn-lg btn-link btn-block btn-register"
                >
                    No Account? Create here!
                </a>
                <p className="mt-5 mb-3 text-muted">
                    &copy; {new Date().getFullYear()}
                </p>
            </form>
        )
    }
}
