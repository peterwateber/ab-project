import React from 'react'
import ReactDOM from 'react-dom'
import {BrowserRouter as Router} from 'react-router-dom'
import Header from '../header'

import '../../../node_modules/react-datetime/css/react-datetime.css'
import '../../../public/css/datettime.css'
import moment from 'moment'
import axios from 'axios'


import { FormFactories } from '../../../libraries/factories'

class Register extends React.Component {
    constructor() {
        super()
        this.postData= this.postData.bind(this)
        this.onChange = this.onChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.state = {
            message: '',
            className: '',
            loading: false,
            hasSubmitted: false,
            formData: {
                first_name: '',
                last_name: '',
                email: '',
                password: '',
                confirm_password: ''
            },
            error: {}
        }
    }
    onChange(e) {

        let formData = {}

        formData[e.target.name] = e.target.value

        if (e.target.name === 'password') {
            if (this.state.formData.confirm_password) {
                formData.confirm_password = this.state.formData.confirm_password
            }
        }

        if (e.target.name === 'confirm_password') {
            formData.password = this.state.formData.password
        }

        if (this.state.hasSubmitted) {
            formData = Object.assign(this.state.formData, formData)
        }

        let newFormObj = {}

        for (var prop in this.state.formData) {
            if (this.state.formData[prop]) {
                newFormObj[prop] = this.state.formData[prop]
            }
        }

        formData = Object.assign(newFormObj, formData)


        this.setState({
            formData: Object.assign(this.state.formData, formData)
        })

        this.postData('single', formData)
    }
    postData(action, passedData) {
        const state = this.state
        let formData = state.formData
        if (passedData) {
            formData = passedData
        }
        formData = Object.assign({
            action: action
        }, formData)
        const self = this
        axios.post('/ajax', formData)
        .then(result => result.data)
        .then(data => {
            setTimeout(() => {
                const error = data.error || {}
                self.setState({
                    error: error,
                    loading: false,
                    message: data.message
                })
                const success = data.success
                if (action === 'single') {
                    if (success) {
                        let errObj = {}
                        for (const prop in passedData) {
                            errObj[prop] = ''
                        }
                        self.setState({
                            error: errObj
                        })
                    }
                }
                if (action === 'register') {
                    if (success) {
                        let resetFormData = {}
                        for (const prop in state.formData) {
                            resetFormData[prop] = ''
                        }
                        self.setState({
                            formData: resetFormData,
                            error: {}
                        })
                    }
                }
            }, 200)
        }).catch(ex => {
            setTimeout(() => {
                self.setState({
                    loading: false
                })
            }, 200)
        })
    }
    handleSubmit(e) {
        e.preventDefault()
        this.setState({
            hasSubmitted: true,
            loading: true,
        })
        this.postData('register')
    }
    render() {
        const loading = this.state.loading ? 'form-loading' : ''
        return (<form autoComplete="off" onSubmit={this.handleSubmit} className={"form-register ".concat(this.state.className)}>
            {
                this.state.message &&
                <div className="alert alert-success">{this.state.message}</div>
            }
            <h1 className="h5 mb-3 font-weight-normal">
                Join us or <a href="/login" className="btn-register">Login here!</a>
            </h1>
            {
                FormFactories.input({
                    id: 'first_name',
                    name: 'first_name',
                    placeholder: 'First Name',
                    onChange: this.onChange,
                    disabled: this.state.loading,
                    message: this.state.error.first_name,
                    value: this.state.formData.first_name
                })
            }
            {
                FormFactories.input({
                    name: 'last_name',
                    id: 'last_name',
                    placeholder: 'Last Name',
                    onChange: this.onChange,
                    disabled: this.state.loading,
                    message: this.state.error.last_name,
                    value: this.state.formData.last_name
                })
            }
            {
                FormFactories.input({
                    name: 'email',
                    id: 'email',
                    placeholder: 'Email Address',
                    onChange: this.onChange,
                    disabled: this.state.loading,
                    message: this.state.error.email,
                    value: this.state.formData.email
                })
            }
            {
                FormFactories.input({
                    id: 'password',
                    type: 'password',
                    name: 'password',
                    placeholder: 'Password',
                    onChange: this.onChange,
                    disabled: this.state.loading,
                    message: this.state.error.password,
                    value: this.state.formData.password
                })
            }
            {
                FormFactories.input({
                    id: 'confirm_password',
                    type: 'password',
                    name: 'confirm_password',
                    placeholder: 'Confirm Password',
                    onChange: this.onChange,
                    disabled: this.state.loading,
                    message: this.state.error.confirm_password,
                    value: this.state.formData.confirm_password
                })
            }
            
            <button disabled={this.state.loading} className={"btn btn-lg btn-success btn-block btn-login ".concat(loading)} type="submit">
                Register
            </button>
            <p className="mt-5 mb-3 text-muted">&copy; {new Date().getFullYear()}</p>
        </form>)
    }
}

class Page extends React.Component {
    render() {
        return (<Router>
            <div>
                <Header></Header>
                <div id="account" className="container mc">
                    <Register/>
                </div>
            </div>
        </Router>)
    }
}

ReactDOM.render(<Page></Page>, document.getElementById('root'))